#author: YU Fan
#school: HKUST
#The script is used to read data in txt files and store it in mongo database

import sys
import pprint
import os
from pymongo import MongoClient
import glob
folder = "/home/yufan/project/benchmark/chart/files/*.txt"
from settings import reportpath,errorpath

def getFiles():
    peak_file= []
    detail_file = []
    for f in glob.glob(folder):
        if '201' in f:
            detail_file.append(f)
        else:
            peak_file.append(f)
    return peak_file,detail_file

def writePeak2Mongo(peak_file):
    client = MongoClient('localhost', 27017)
    db = client['benchmark']
    peak_collection = db['peak_benchmark']
    for p in peak_file:
        print("data file: "+p)
        basename = os.path.basename(p)
        tool = basename[:-4]
        pf = open(p)
        content = pf.readlines()

        print(content)
        for line in content:
            row = line.split(',')
            c = {"tool": tool,
                 "time": row[0],
                 "cpu": row[1],
                 "mem": row[2],
                 "wall": row[3],
                 "bugs": row[4][:-1]}
            collection_id = peak_collection.insert_one(c).inserted_id
            pprint.pprint(c)
            print("Collection id in peak collection of MongoDB is " + str(collection_id))

def writeDetail2Mongo(detail_file):
    client = MongoClient('localhost', 27017)
    db = client['benchmark']
    detail_collection = db['detail_benchmark']
    for d in detail_file:
        print("data file: "+d)
        basename = os.path.basename(d)
        basename = basename[:-4]
        k = basename.rfind("_")
        tool = basename[:k]
        time = basename[k + 1:]
        k = tool.rfind("+")
        project_name = tool[:k]
        bc_file = tool[k + 1:]

        df = open(d)
        data = df.read()
        output = ""
        error = ""
        if os.path.isfile(errorpath+basename+"_error.txt"):
            output_result = open(errorpath+basename+"_error.txt")
            output_value = output_result.readlines()
            error_index = output_value.index("error:\n")
            output_list = output_value[:error_index]
            error_list = output_value[error_index+1:]
            for o in output_list:
                output += o
            for e in error_list:
                error += e
        bugs = ""
        bug_types = ""
        if os.path.isfile(reportpath + basename + "_report.txt"):
            report_result = open(reportpath + basename + "_report.txt")
            bugs = report_result.read()

        if os.path.isfile(reportpath + basename + "_bugtype.txt"):
            bug_result = open(reportpath + basename + "_bugtype.txt")
            bug_types = bug_result.read()

        c = {"tool": tool,
             "project": project_name,
             "bc": bc_file,
             "time": time,
             "data": data,
             "output": output,
             "error": error,
             "bugs": bugs,
             "bug_types": bug_types}
        collection_id = detail_collection.insert_one(c).inserted_id
        pprint.pprint(c)
        print("Collection id in peak collection of MongoDB is " + str(collection_id))

def main(args= None):

    peak,detail = getFiles()
    writePeak2Mongo(peak)
    writeDetail2Mongo(detail)


if __name__ == '__main__':
    sys.exit(main())