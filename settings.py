#author: YU Fan
#school: HKUST
import os
current_dir = os.path.dirname(os.path.abspath(__file__))
chmod = 'chmod +x configure'
configure = "./configure"
bc_dir1 = "/.piggy/merged_top/"
bc_dir2 = "/.piggy/top/"
timelimit = "1000000000s"
hardtimelimit = "1000000000s"
memlimit = "1000000000MB"
cpuCores = "8"
#pp_capture = "/var/lib/jenkins/workspace/cap-build/pp-capture make -j8"
pp_capture_default = current_dir + "/pinpoint/cap_build/pp-capture make -j8"
pp_capture = current_dir + "/pinpoint/cap_build/pp-capture "
pp_capture_job = " -j8"
pp_report = "/home/yufan/pp-report/pp-report --user=yufan --passwd=712803 --remote-port=40098 --upload="
#pp_report = current_dir + "/pinpoint/bin/pp-report --user=yufan --passwd=712803 --remote-port=40098 --upload="
#pp_check = "/var/lib/jenkins/workspace/build_pinpoint/build/Release+Asserts/bin/pp-check"
pp_check = current_dir + "/pinpoint/bin/pp-check"

nworkers = "-nworkers=10"
report = "-debug-report -report="
load = "-load=" +current_dir+ "/pinpoint/ppcheck/plugin/Vulnerabilities.so"
ps = "-ps-all"

chart_console = current_dir+"/chart/console.txt"

filepath = current_dir+"/chart/files/"
errorpath = current_dir+"/chart/error/"
reportpath = current_dir+"/chart/report/"
tools = current_dir+"/chart/tool.txt"
toolpath = current_dir+"/chart/tool/"
clone = "git clone "
pinpoint = "pinpoint.zip"
pppasswd = "password.txt"
scp_pinpoint = "sshpass -p fyuac712803 scp yufan@chcpu9.cse.ust.hk:/var/lib/jenkins/workspace/docker_pack/"+pinpoint+" "+current_dir
scp_pppasswd = "sshpass -p fyuac712803 scp yufan@chcpu9.cse.ust.hk:/var/lib/jenkins/workspace/docker_pack/"+pppasswd+" "+current_dir
wget_pinpoint = "wget --user=dahuilang --password=xiaohuilang -P "
pinpoint_url = " http://chcpu9.cse.ust.hk:40071/pinpoint/pinpoint-GNU/"

database = "benchmark"
peak_collection = "peak_benchmark"
detail_collection = "detail_benchmark"
