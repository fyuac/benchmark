#!/usr/bin/python3
#author: YU Fan
#school: HKUST
#The script is used for benchmarking the pp-check process

import sys
import hashlib
import subprocess
import psutil
import time
import shlex
import pymysql
import datetime
import os
import requests
import storeInDatabase
import util
from settings import current_dir,pp_report,filepath,errorpath,reportpath,tools,toolpath,database,peak_collection,detail_collection
import json
import glob
from pymongo import MongoClient
import pprint
TIME_PRECISION = 2
MEGA = 1024*1024 #transfer byte to megabyte
MILLI = 1000   #transfer second to millisecond
class BenchmarkData(object):
    """
    The class Benchmark manages the import of source files, options, columns and
    the tool from a benchmark_file.
    This class represents the <benchmark> tag.
    """

    def __init__(self,args,tool_name,start_time,env,cwd,pre_subprocess):
        self.args = args
        self.env = env
        self.cwd = cwd
        self.pre_subprocess = pre_subprocess
        self.current_dir = current_dir
        self.tool_name = tool_name
        self.start_time = start_time
        self.pid = None
        self.proc = None
        with open(current_dir + '/master.txt', 'r') as f:
            line = f.read()
        content = line.split("\n")
        self.master_directory = content[0]


    def get_cputime(self,p):
        #p = psutil.Process(self.pid)
        time = p.cpu_times()

        #for child in p.children(recursive=True):
         #   child_time = child.cpu_times()
          #  time[0] += child_time[0]
           # time[1] += child_time[1]
        cputime = time[0] + time[1]+time[2]+time[3]#user time + system time
        #total = os.times()
        #return p.cpu_percent() *MILLI *(total[0])
        return cputime*MILLI

    def get_memory(self,p):
        #p = psutil.Process(self.pid)
        memory = p.memory_info()
        rrs = memory[0]#physical memory
        vms = memory[1]#virtual memory
        #for child in p.children(recursive=True):
         #   child_mem = child.memory_info()
          #  rrs += child_mem[0]
           # vms += child_mem[1]
        return rrs/MEGA,vms/MEGA


    def get_value_with_default(self,dictionary, key, default_val=""):
        if key in dictionary:
            return dictionary[key]
        else:
            return default_val

    def compute_report_md5(self,rp,src):
        md5 = hashlib.md5()
        md5.update(str(self.get_value_with_default(rp, "Dominated")).encode('utf-8'))
        md5.update(str(self.get_value_with_default(rp, "Valid")).encode('utf-8'))

        for step in rp["DiagSteps"]:
            md5.update(str(self.get_value_with_default(step, "Line")).encode('utf-8'))
            md5.update(str(src[int(step["File"])]).encode('utf-8'))
            md5.update(self.get_value_with_default(step, "Tip").encode('utf-8'))
            md5.update(self.get_value_with_default(step, "Function").encode('utf-8'))

            inst = self.get_value_with_default(step, "Inst")
            if inst is not None:
                index = inst.rfind("!dbg")
                if index >= 0:
                    inst = inst[:index]
                md5.update(inst.encode('utf-8'))

        return md5.hexdigest()
    #discarded function
    def write_detail_data(self, tool, time, data, out, err,bugs,bug_types):
        succ = False
        try:
            payload = {'data': data,
                       'out': out,
                       'err': err,
                       'time': time,
                       'tool': tool,
                       'bugs':bugs,
                       'bugtype': bug_types,
                      }
            url = 'http://chcpu9.cse.ust.hk:40099/mybenchmark'
            r = requests.post(url, data=payload)
            if r.status_code == requests.codes.ok:
                succ = True
            errmsg = r.text

        except Exception as e:
            errmsg = "Upload Error: Cannot connect to remote server chcpu9"

        print("\n" + errmsg)
    #To get the tools that has been configured. Now I store the configured tools' names in a simple txt file called tools.txt.
    #Maybe we can store it in mongo database.
    def getTools(self):
        with open(tools) as f:
            content = f.readlines()
            tool_name = []
            for line in content:
                row = line.split("\n")
                tool_name.append(str(row[0]))
        return tool_name

    # To get the bc files for a configured project. Now I store the configured files' names in simple txt files in the folder tool/.
    # Maybe we can store it in mongo database.
    def getBCFiles(self,project_name):
        with open(toolpath + project_name + ".txt", "r+") as f:
            content = f.readlines()
            bc_files = []
            for line in content:
                row = line.split("\n")
                bc_files.append(str(row[0]))
        return bc_files

    #store the detailed benchmark data in txt files
    def store_detail_data(self, tool_name, start_time, data, output, error, bugs, bug_types):
        file_name = tool_name + "_" + str(start_time)
        data_file = filepath + file_name + ".txt"
        error_info = errorpath + file_name + "_error.txt"
        pp_report = reportpath + file_name + "_report.txt"
        pp_bug_type = reportpath + file_name + "_bugtype.txt"

        k = tool_name.rfind("+")
        project_name = tool_name[:k]
        bc_file = tool_name[k + 1:]

        if project_name not in self.getTools():
            with open(tools, 'a+') as f:
                f.write(project_name)
                f.write('\n')

        if bc_file not in self.getBCFiles(project_name):
            with open(toolpath + project_name + ".txt", 'a+') as f:
                f.write(bc_file)
                f.write('\n')
                print("write bc " + bc_file + " into file " + toolpath + project_name + ".txt")

        out_err = open(error_info, 'a+')
        out_err.write("output:\n")
        out_err.write(output)
        out_err.write("\n")
        out_err.write("error:\n")
        out_err.write(error)

        normal_file = open(data_file, 'a+')
        normal_file.write(data)

        report = open(pp_report, 'a+')
        report.write(bugs)

        bugtype = open(pp_bug_type, 'a+')
        bugtype.write(bug_types)

    #store the detailed data in mongo database
    def store_detail_data_mongodb(self, tool_name, start_time, data, output, error, bugs, bug_types):
        client = MongoClient('localhost', 27017)
        db = client[database]
        collection = db[detail_collection]
        k = tool_name.rfind("+")
        project_name = tool_name[:k]
        bc_file = tool_name[k + 1:]
        c = {"tool": tool_name,
             "project":project_name,
             "bc":bc_file,
             "time": start_time,
             "data": data,
             "output": output,
             "error": error,
             "bugs": bugs,
             "bug_types":bug_types}

        collection_id = collection.insert_one(c).inserted_id
        print("Collection id in detail collection of MongoDB is " + str(collection_id))

    #discarded function
    def write_peak_data(self,tool_name, exec_time, cputime, memory, walltime, total_bugs):
        succ = False
        try:
            payload = {'tool': tool_name,
                       'time': str(exec_time),
                       'cpu': cputime,
                       'mem': memory,
                       'wall': walltime,
                       'bugs': total_bugs,

                       }
            url = 'http://chcpu9.cse.ust.hk:40099/peakbenchmark'
            r = requests.post(url, data=payload)
            if r.status_code == requests.codes.ok:
                succ = True
            errmsg = r.text

        except Exception as e:
            errmsg = "Upload error: Cannot connect to remote server"

        print("\n" + errmsg)

    #store peak benchmark data in txt files
    def store_peak_data(self, tool, time, cpu, mem, wall, bugs):
        file_name = filepath + tool + ".txt"
        f = open(file_name, 'a+')
        f.write(str(time) + ",")
        f.write(str(cpu) + ",")
        f.write(str(mem) + ",")
        f.write(str(wall) + ",")
        f.write(str(bugs) + "\n")

    #store peak data in mongo database
    def store_peak_data_mongodb(self, tool, time, cpu, mem, wall, bugs):
        client = MongoClient('localhost', 27017)
        db = client[database]
        collection = db[peak_collection]
        c = {"tool": tool,
             "time": time,
             "cpu": cpu,
             "mem": mem,
             "wall": wall,
             "bugs": bugs}

        collection_id = collection.insert_one(c).inserted_id
        print("Collection id in peak collection of MongoDB is " + str(collection_id))

    #upload bc report files
    def upload_brf(self,project_name,bc_name):
        if not os.path.exists(self.master_directory + project_name + "/.piggy/report/"):
            os.makedirs(self.master_directory + project_name + "/.piggy/report/")
        with open(self.master_directory + project_name + "/.piggy/report/"+bc_name+"_report.txt", 'r') as f:
            report_file = json.load(f)
        brf = self.master_directory + project_name + "/.piggy/report/"+ bc_name + "_report_"+str(self.start_time)+".txt"
        print("try to load report")
        with open(brf, 'a+') as f:
            json.dump(report_file, f)
        print("load report end")
        pp_report_cmd = pp_report+project_name+" "+brf
        report = subprocess.Popen(pp_report_cmd.split())
        report.communicate()

    #execute the benchmark process
    def execute_mybenchmark(self):
        start = util.read_monotonic_time()
        print("\nbenchmark for "+self.tool_name+" begins!")
        #The subprocess has a bug: the subprocess PIPE can only store certain number of characters.
        #So we can write the characters in a txt file first and then store it to the database.
        output_buffer = open(self.current_dir+"/output_buffer_"+self.tool_name+"_"+str(self.start_time)+".txt","w")
        self.proc  = subprocess.Popen(self.args,
                     stdin=None,
                     stdout=output_buffer,
                     stderr=subprocess.PIPE,
                     universal_newlines=True,
                     env=self.env, cwd=self.cwd,
                     close_fds=True,
                     preexec_fn=self.pre_subprocess
                     )
        self.pid = self.proc.pid
        data=""
        p = psutil.Process(self.pid)
        max_cpu = 0
        max_mem = 0
        while self.proc.poll() == None :

            cpu = self.get_cputime(p)
            rrs,vms  = self.get_memory(p)
            if max_cpu < cpu:
                max_cpu = cpu
            if max_mem < rrs:
                max_mem = rrs
            data += str(cpu) + "," + str(rrs) + "," + str(vms) + "\n"
            time.sleep(0.1)
        #self.write_file(cputime,physical_memory,virtual_memory,memory)
        #self.write_error_information(self.proc)
        #self.store_data()
        wall = util.read_monotonic_time() - start
        print("process id:")
        print(str(self.pid))
        print("wall time")
        print(str(wall))
        output, error = self.proc.communicate()
        read_output_buffer = open(self.current_dir+"/output_buffer_"+self.tool_name+"_"+str(self.start_time)+".txt","r")
        real_output = read_output_buffer.read()
        k = self.tool_name.rfind("+")
        project_name = self.tool_name[:k]
        bc_name = self.tool_name[k + 1:]
        with open(self.master_directory + project_name + "/.piggy/report/"+bc_name+"_report.txt", 'r') as f:
            report_file = json.load(f)
        total_bugs = report_file["TotalBugs"]
        src_file = report_file["SrcFiles"]
        bugs = ""
        bug_types = ""
        for bugType in report_file["BugTypes"]:
            desc = bugType["Description"]
            total_reports = bugType["TotalReports"]
            bug_types += str(desc)+ "+" + str(total_reports)+ "\n"
            for report in bugType["Reports"]:
                md5 = self.compute_report_md5(report,src_file)
                steps = report["DiagSteps"]
                first_line = steps[0]["Line"]
                error_function = steps[0]["Function"]
                last_line = steps[-1]["Line"]
                bugs += str(error_function) + "," + str(first_line) + "," + str(last_line) + "," + str(md5) + "\n"

        # Now we not only store the data in mongo database but also store it in txt files.
        # In the past there are some problems in mongo database of the server.
        self.store_detail_data(self.tool_name, str(self.start_time), data, real_output, error,bugs,bug_types)
        self.store_peak_data(self.tool_name,str(self.start_time),max_cpu,max_mem,wall,total_bugs)

        self.store_detail_data_mongodb(self.tool_name, str(self.start_time), data, real_output, error, bugs, bug_types)
        self.store_peak_data_mongodb(self.tool_name, str(self.start_time), max_cpu, max_mem, wall, total_bugs)

        self.upload_brf(project_name,bc_name)
        #delete useless files
        print("deleting useless files...")
        for file in glob.glob(current_dir + "/output_buffer_*"):
            print(file)
            os.remove(file)


