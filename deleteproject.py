from settings import current_dir
import os
import argparse
import sys
import subprocess
with open(current_dir+'/master.txt','r') as f:
    line = f.read()
content = line.split("\n")
master_directory = content[0]

directories  = [os.path.join(master_directory, f)+'/' for f in os.listdir(master_directory) if os.path.isdir(os.path.join(master_directory, f))]

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--delete", help="to delete a project in the master directory (only specify project name)",required=True)
args = parser.parse_args()

if args.delete:
    tool = args.delete
    path = os.path.join(master_directory, args.delete)

    if os.path.isdir(path)==False:
        sys.exit("Error: The project "+path+" does not exist !")
    if path[-1:] != '/':
        path += '/'
    args = "rm -r "+path
    p = subprocess.Popen(args.split())
    p.communicate()
    if path in directories:
        directories.remove(path)
        with open(current_dir + '/configured.txt', 'w') as f:
            for n in directories:
                f.write(n + "\n")
    json_file = current_dir + '/json/'
    results_file = current_dir + '/results/'
    kill_json_files = "find "+json_file+" -name '"+tool+"*' -delete"
    kill_results_files = "find " + results_file + " -name '" + tool + "*' -delete"
    print(kill_json_files)
    print(kill_results_files)
    os.system(kill_json_files)
    os.system(kill_results_files)
