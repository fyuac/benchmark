import json
import os
def readBuildInformation(build_file):
    print(build_file)
    ifexist = os.path.isfile(build_file)
    projects = []
    project_name = []
    if ifexist:
        with open(build_file, 'r') as f:
            build = json.load(f)
        projects = build["projects"]
        project_name = []
        for p in projects:
            project_name.append(p["project_name"])
    return projects,project_name
