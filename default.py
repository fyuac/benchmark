import argparse
import sys
import os
from settings import current_dir
parser = argparse.ArgumentParser()
parser.add_argument("-m", "--master", help="to define the default master directory that contains software projects")
parser.add_argument("-s", "--setting", help="settings for pp-check, e.g. -ps-ml")
args = parser.parse_args()
with open(current_dir+'/master.txt','r') as f:
    line = f.read()
content = line.split("\n")
master_directory = content[0]
pp_setting = content[1]
new_directory = master_directory
new_setting = pp_setting
if args.master:
    new_directory = args.master
    if os.path.isdir(new_directory) == False:
        sys.exit("Error: This master directory "+new_directory+" is invalid !")
    if new_directory[-1:] != '/':
        new_directory += '/'
    print("overwrite master directory, from")
    print(master_directory)
    print("to")
    print(new_directory)

if args.setting:
    new_setting = args.setting
    print("overwrite pp-check setting, from")
    print("-"+pp_setting)
    print("to")
    print("-"+new_setting)

with open(current_dir + '/master.txt', 'w') as f:
     f.write(new_directory+"\n"+new_setting)
