#!/usr/bin/python3
#author: YU Fan
#school: HKUST
#This script is used to update pinpoint, configure projects, run pp-check for all specified bc files and store the benchmark data.
import glob
import os
import subprocess
import shlex
import json
import sys
import zipfile
from pyunpack import Archive
import datetime
from settings import current_dir,pp_capture,configure,bc_dir1,bc_dir2,timelimit,memlimit,hardtimelimit,cpuCores,pp_check,nworkers,load,report,chmod,pp_capture_default,pp_capture_job,chart_console,clone,pinpoint,pppasswd,pinpoint_url,scp_pinpoint,scp_pppasswd
import psutil
import pputil
#write the information to a file so that the website can read the information
def writeConsole(info):
    with open(chart_console,'a+') as f:
        f.write(str(info))
    f.close()
#terminate the benchmark process
def terminateBenchmark():
    with open(current_dir + "/parent_process.txt") as f:
        ppid = f.read()
    proc = psutil.Process(ppid)
    proc.terminate()

#update pinpoint
def updatePinpoint():
    writeConsole("Updating pinpoint...\n")
    get_pinpoint = scp_pinpoint
    get_password = scp_pppasswd
    print(get_pinpoint)
    p1 = subprocess.Popen(get_pinpoint.split())
    p1.communicate()
    print(get_password)
    p2 = subprocess.Popen(get_password.split())
    p2.communicate()
    if os.path.isfile(current_dir+"/"+pinpoint):
        filetime = datetime.datetime.fromtimestamp(os.path.getmtime(current_dir+"/"+pinpoint))
        print("last modified time is " + str(filetime))
        print("difference is " + str(datetime.datetime.now() - filetime))
        delta = datetime.datetime.now() - filetime
        ifexist = os.path.isdir(current_dir+"/pinpoint")
        if delta < datetime.timedelta(days=1) or ifexist is False:
            remove_pinpoint = "rm -rf "+current_dir+"/pinpoint"
            rp = subprocess.Popen(remove_pinpoint.split())
            rp.communicate()
            with open(current_dir+"/"+pppasswd) as p:
                passwd = p.read()
            print(passwd)
            unzip_pinoint = "unzip -P "+str(passwd)+" -d "+current_dir+" "+current_dir+"/"+pinpoint
            uz = subprocess.Popen(unzip_pinoint.split())
            uz.communicate()
        os.remove(current_dir + "/"+pppasswd)
        os.remove(current_dir + "/"+pinpoint)
        if ifexist is False:
            library = current_dir+"/pinpoint/install.sh"
            lib = subprocess.Popen(library)
            lib.communicate()

#extract all compressed files in the server
def extractFiles(master_directory):
    writeConsole("Extracting compressed files...\n")
    directories = [os.path.join(master_directory, f) for f in os.listdir(master_directory) if
                   os.path.isdir(os.path.join(master_directory, f))]
    zip_format = ['zip', 'rar', 'tar', 'xz', 'gz', '7z']
    zip_files = []
    for f in zip_format:
        files = glob.glob(master_directory + '*.' + f)
        for f in files:
            zip_files.append(f)
    for zip_filename in zip_files:
        basename = zip_filename.split(os.extsep)
        basename = basename[0]
        if basename not in directories:
            dirname,filename = os.path.split(zip_filename)
            Archive(zip_filename).extractall(dirname)
    for zip_filename in zip_files:
        os.remove(zip_filename)

#run the whole benchmark process
def main(args= None):
    if os.path.isfile(chart_console):
        os.remove(chart_console)
    updatePinpoint()
    with open(current_dir+'/master.txt','r') as f:
        line = f.read()
    content = line.split("\n")
    master_directory = content[0]
    ps = "-"+content[1]

    extractFiles(master_directory)

    unconfigred_projects = []
    new_configured_projects = []
    #projects specified in the json file
    build_projects, build_projects_name = pputil.readBuildInformation(master_directory + 'build.json')

    #download the project from the link
    for b in build_projects:
        if b["link"] != None:
            if not os.path.isdir(master_directory + b["project_name"]):
                download_project = clone + b["link"] + " " + master_directory + b["project_name"]
                download_proc = subprocess.Popen(download_project.split())
                download_proc.communicate()

    directories  = [os.path.join(master_directory, f) for f in os.listdir(master_directory) if os.path.isdir(os.path.join(master_directory, f))]
    with open(current_dir+'/configured.txt','r') as f:
        configured_projects = shlex.split(f.read(),'\n')

    for d in directories:
        if d in configured_projects:
            new_configured_projects.append(d)
        else:
            for b in build_projects:
                if d[-1] == '/':
                    basename = os.path.basename(d[:-1])
                else:
                    basename = os.path.basename(d)
                if basename == b["project_name"]:
                    unconfigred_projects.append({'directory': d,
                                                 "project_name": b["project_name"],
                                                 "capture_commands": b["capture_commands"],
                                                 "build_commands": b["build_commands"],
                                                 "check_option": b["check_option"]
                                                 })



    #unconfigred_projects.remove(master_directory+"bug_reports")
    print("unconfig projects")
    print(unconfigred_projects)
    #configure the projects that has not yet been built
    for u in unconfigred_projects:
        print(u)
        dir = u['directory']
        build_cmd = u["build_commands"]
        capture_cmd = u["capture_commands"]

        if u["check_option"] != None :
            check_cmd = u["check_option"]
        else:
            check_cmd = ps

        writeConsole('Configuring for project '+u["project_name"]+'\n')
        if build_cmd != None and build_cmd[0]!="no":
            for c in build_cmd:
                ccmd = subprocess.Popen(c.split(),cwd=dir)
                ccmd.communicate()
        elif build_cmd == None:
            chmod_project = subprocess.Popen(chmod.split(),cwd=dir)
            chmod_project.communicate()
            configure_project = subprocess.Popen(args=configure,cwd=dir)
            configure_project.communicate()

        writeConsole('Run pp-capture for project ' + u["project_name"]+'\n')
        if capture_cmd != None:
            cap_cmd = pp_capture+capture_cmd+pp_capture_job
            print("default= "+pp_capture_default)
            print("now    = "+cap_cmd)
            ccmd = subprocess.Popen(cap_cmd.split(), cwd=dir)
            ccmd.communicate()
        else:
            capture_project = subprocess.Popen(args=shlex.split(pp_capture_default),cwd=dir)
            capture_project.communicate()

        new_configured_projects.append(dir)
        bc_files = [os.path.join(dir + bc_dir1, f) for f in os.listdir(dir + bc_dir1) if os.path.isfile(os.path.join(dir + bc_dir1, f))]
        if len(bc_files) == 0:
            bc_files = [os.path.join(dir + bc_dir2, f) for f in os.listdir(dir + bc_dir2) if os.path.isfile(os.path.join(dir + bc_dir2, f))]
        project_name = os.path.basename(os.path.normpath(dir))

        if not os.path.exists(master_directory + project_name+"/.piggy/report/"):
            os.makedirs(master_directory + project_name+"/.piggy/report/")

        #Write configure files for all bc fils.
        for bc in bc_files:
            bc_name = os.path.basename(os.path.normpath(bc))[:-3]

            data = {
                "benchmark": {
                    "@tool": "test",
                    "@timelimit": timelimit,
                    "@hardtimelimit": hardtimelimit,
                    "@memlimit": memlimit,
                    "@cpuCores": cpuCores,
                    "cmd": {
                        "@name": "pp-check cmd",
                        "#text": " ".join([pp_check,nworkers,report+master_directory +project_name+"/.piggy/report/"+bc_name+"_report.txt",load,bc,check_cmd])
                    },
                    "rundefinition": None,
                    "tasks": [
                        {
                            "@name": project_name+"+"+bc_name,
                            "withoutfile": [
                                "task "+project_name+"+"+bc_name
                            ]
                        }
                    ]
                }
            }
            with open(current_dir + "/json/"+project_name+"+"+bc_name+".json", 'w') as f:
                json.dump(data, f)
        print("The configure project is " + dir)

    with open(current_dir+'/configured.txt','w') as f:
        for n in new_configured_projects:
            f.write(n+"\n")

    path = current_dir+"/json/*.json"
    script = "python3 "+current_dir+"/benchexec.py "

    #Run all benchmark process
    for file in glob.glob(path):
        print(file)
        basename = os.path.basename(file)
        basename = basename[:-5]
        t = basename.rfind("+")
        bcname = basename[t + 1:]
        projectname = basename[:t]
        if projectname in build_projects_name:
            writeConsole("Run benchmark for file " + projectname+ " : " + bcname+'.bc\n')
            proc = subprocess.Popen((script+file).split())
            proc.communicate()
        #os.system(script+file)

    #delete useless files
    for root, dirs, files in os.walk(current_dir + "/results/", topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))


if __name__ == '__main__':
    sys.exit(main())