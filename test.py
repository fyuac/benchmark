from settings import toolpath,current_dir
import os

with open(current_dir + '/master.txt', 'r') as f:
    line = f.read()
content = line.split("\n")
master_directory = content[0]
ps = "-" + content[1]
def getBCFiles(project_name):
    with open(toolpath + project_name + ".txt", "r+") as f:
        content = f.readlines()
        bc_files = []
        for line in content:
            row = line.split("\n")
            bc_files.append(str(row[0]))
    return bc_files

def setBCFiles(project_name,newbc):
    with open(toolpath + project_name + ".txt", "w") as f:
        for bc in newbc:
            f.write(bc+'\n')

directories  = [os.path.join(master_directory, f) for f in os.listdir(master_directory) if os.path.isdir(os.path.join(master_directory, f))]
basename = []
for d in directories:
    b = os.path.basename(d)
    basename.append(b)
for b in basename:
    bcfiles = getBCFiles(b)
    new_bcfiles = list(set(bcfiles))
    setBCFiles(b,new_bcfiles)
