#!/bin/bash

test_python_type() {
    #test whether this python is 2.7 and >= 2.7.6 and wide
    local cmd=$1
    info=`$cmd -c "import sys; print sys.maxunicode"`
    if [ "$info" == "1114111" ]
    then
	V1=`$cmd -V 2>&1 | awk '{print $2}' | awk -F '.' '{print $1}'`
	V2=`$cmd -V 2>&1 | awk '{print $2}' | awk -F '.' '{print $2}'`
	if [[ $V1 -eq 2 && $V2 -eq 7 ]]
	then
	    return 0
	else
	    return 1
	fi
    else
	return 1
    fi
}

install_python() {
   local CONFIGURE_CMD="./configure --enable-unicode=ucs4 --prefix=${INSTALLER_PYTHON_PATH}"
   local PYTHON_PATH=$1
   local MAKE_CMD="/bin/bash -c \"cd ${INSTALLER_PYTHON_PATH} && $CONFIGURE_CMD && make && make altinstall\""
   (eval $MAKE_CMD) || return 1
   return 0
}

basedir=$PWD/$(dirname "$0")
script_name="install.pyc"
script_location=`dirname $(readlink -f $0)`

help_info="Python 2.7 is not installed, you need to export the directory of Python 2.7 to PATH environment varible. For example, if your Python is installed at /path/to/python, you should type 'export PATH=/path/to:\$PATH'. "
INSTALLER_PYTHON_PATH=${script_location}/Python
ERR_NOTICE=$help_info"\nFail to install Python 2.7. Stoped.\n"

if command -v ${INSTALLER_PYTHON_PATH}/bin/python2.7 >/dev/null 2>&1
then
    command=$INSTALLER_PYTHON_PATH/bin/python2.7
elif command -v python2.7 >/dev/null 2>&1
then
    command="python2.7"
    test_python_type $command || { install_python $INSTALLER_PYTHON_PATH && command=$INSTALLER_PYTHON_PATH/bin/python2.7 || { echo -e $ERR_NOTICE; exit 1;} }
elif command -v python >/dev/null 2>&1
then
    command="python"
    test_python_type $command || { install_python $INSTALLER_PYTHON_PATH && command=$INSTALLER_PYTHON_PATH/bin/python2.7 || { echo -e $ERR_NOTICE; exit 1;} }
fi

if [ -z "$command" ] 
then
    install_python $INSTALLER_PYTHON_PATH || { echo $help_info; exit 1;}
    command=$INSTALLER_PYTHON_PATH/bin/python2.7
fi

(eval $command $basedir/$script_name) && echo "Now Pinpoint is successfully installed, if Python 2.7 is not installed on your system before, you should use ${INSTALLER_PYTHON_PATH}/bin/python2.7 instead. "
exit 0

