#!/bin/bash

set_environ() {
    script_location=`dirname $(readlink -f $0)`
    local cmd=""
    local HELP="Python 2.7 is not installed, you need to export the directory of Python 2.7 to PATH environment varible. For example, if your Python is installed at /path/to/python, you should type 'export PATH=/path/to:\$PATH'. "
    # search Python
    if command -v ${script_location}/Python/bin/python2.7 >/dev/null 2>&1
    then
	cmd=${script_location}/Python/bin/python2.7 
    elif command -v python2.7 >/dev/null 2>&1
    then
	cmd=`type -p python2.7`
    elif command -v python >/dev/null 2>&1
    then
	V1=`python -V 2>&1 | awk '{print $2}' | awk -F '.' '{print $1}'`
 	V2=`python -V 2>&1 | awk '{print $2}' | awk -F '.' '{print $2}'`
	if [[ $V1 -eq 2 && $V2 -eq 7 ]]
	then
	    cmd=`type -p python`
	fi
    fi
    # if Python not found, then exit.
    if [ -z "$cmd" ]
    then
	echo $HELP
	exit 1
    fi
    PYTHON_PATH=$cmd
}

run_ppcapture() {
    local workdir=$1
    echo "Start Capture Process"
    # start capture and build
    local CAPTURE_PATH=`dirname $(readlink -f $0)`/pp-capture
    /bin/bash -c "cd $workdir && ${PYTHON_PATH} ${CAPTURE_PATH} $capture_options"
    return $?
}

run_ppcheck() {
    # Start analysis process
    local BCDIR=$1/.piggy/merged_top
    echo "Start Analysis Process"
    local CHECK_PATH=`dirname $(readlink -f $0)`/pp-check
    local TEST_SCRIPT_PATH=`dirname $(readlink -f $0)`/../ppcheck/bin/runtest.py
    test -f $TEST_SCRIPT_PATH || { TEST_SCRIPT_PATH=`dirname $(readlink -f $0)`/../ppcheck/bin/runtest.pyc; }
    local threads=$2
    local smtd_options=$3
    local ret=0
    if [ -n "$smtd_options" ] 
    then
	local SMTD_PATH=`dirname $(readlink -f $0)`/pp-smtd
	local SMTD_PORT="34856"
	/bin/bash -c "${PYTHON_PATH} ${SMTD_PATH} ${smtd_options} -stmd-key=$SMTD_PORT &" && \
	export PPCHECK=$CHECK_PATH && ${PYTHON_PATH} ${TEST_SCRIPT_PATH} $BCDIR --merge-reports --threads=$threads --pinpoint-args "$check_options -solver-enable-smtd=$SMTD_PORT"
	ret=$?
	killall -u `whoami` pp-smtd || true
    else
	export PPCHECK=$CHECK_PATH && ${PYTHON_PATH} ${TEST_SCRIPT_PATH} $BCDIR --merge-reports --threads=$threads --pinpoint-args "$check_options"
	ret=$?
    fi
    return $ret
}

run_ppreport() {
    local BCDIR=$1/.piggy/merged_top
    # Bug reports server information
    local PORT=40072
    local REPORT_PATH=`dirname $(readlink -f $0)`/pp-report
    echo -e "Now ready to start pp-report.\n" 
    # Bug reports
    local MODULE_REPORTS=`ls $BCDIR/*_br.txt`
    rc=$?
    if [[ $rc != 0 ]]; then
	echo "-----------------------------------------------------------"
	echo "Cannot find Pinpoint analysis bug reports. Abort."
	return $rc;
    fi
    #MERGED_REPORT=$BCDIR/ALL-MODULE-RESULTS
    #mv $BCDIR/merged_report.txt $MERGED_REPORT
    ${PYTHON_PATH} ${REPORT_PATH} --builddir $BCDIR/../../ $report_options --module-suffix \"bc_br.txt\" $BCDIR/merged_report.txt $MODULE_REPORTS || return $?
    echo "...................................................."
    echo "Pinpoint finished analysis and uploding bug report."
    return 0
}

# first parse args
while getopts :t:b:c:r:s:d: opt
do
    case "$opt" in
    t) threads=$OPTARG ;;
    b) capture_options=" $OPTARG" ;;
    c) check_options=" $OPTARG" ;;
    r) report_options=" $OPTARG" ;;
    s) smtd_options=" $OPTARG" ;;
    d) WORKDIR=$OPTARG
    esac
done

if [ -z "$threads" ] || [ -z "$capture_options" ] && [ -z "$check_options" ] && [ -z "$report_options" ]
then
    echo -e "Usage: \n"
    echo -e "-t args	: number of pp-check you want to run simultaneously. "
    echo -e "-b \"args\"	: Options passed to pp-capture. pp-capture will not run if this option is not specified."
    echo -e "-c \"args\"	: Options passed to pp-check. pp-check will not run if this option is not specified."
    echo -e "-r \"args\"	: Options passed to pp-report. pp-report will not run if this option is not specified."
    echo -e "-d \"args\"	: The path of working directory where Pinpoint works on it. "
    echo -e "-s \"args\"	: Use pp-smtd and all values after '-s' will be passed to pp-smtd. pp-smtd will not run if this option is not specified."
    echo -e "\nNotice: Please don't write blank space in the value of an option that passed to pp-report, pp-capture, pp-smtd or pp-check. For example, you should type [./run-pinpoint.sh -r \"--upload untitled-project\"] in the terminal instead of [./run-pinpoint.sh -r \"--upload untitled project\"]\n"
    echo -e "Usage example: ./run-pinpoint.sh -t 4 -r \"--upload projectname --user test --passwd 123456\" -b \"--always-build -- make -j4\" -c \"-ssu-all -ps-all\" -d \"/path/to/yourproject\"\n"
    exit 1
fi

if [ -z "$WORKDIR" ]
then
    WORKDIR=$PWD
fi

echo "PP-CAPTURE OPTIONS: $capture_options"
echo "PP-CHECK OPTIONS: $check_options"
echo "PP-REPORT OPTIONS: $report_options"
echo "WORKDIR: $WORKDIR"


# use the runtest script to generate reports for all bcs
echo "Welcome to use Pinpoint, the most advanced static analysis tool."
WORKDIR=`readlink -f $WORKDIR`

set_environ $0
if [ -n "$capture_options" ]
then
    run_ppcapture $WORKDIR || exit $?
fi
if [ -n "$check_options" ]
then
    if [ -n "$smtd_options" ]
    then
	run_ppcheck $WORKDIR $threads $smtd_options
    else
	run_ppcheck $WORKDIR $threads
    fi
fi
if [ -n "$report_options" ]
then
    run_ppreport $WORKDIR || exit $?
fi
