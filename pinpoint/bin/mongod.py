#!/usr/bin/env python
import os
import sys
import subprocess
import argparse
import signal
import fcntl


def start_mongodb(bin_path, data_dir, port=None):
    if os.path.isfile(bin_path) is False:
        print 'Command mongod not found.'
        return 2
    data_dir = os.path.abspath(data_dir)
    data_dir = os.path.join(data_dir, 'data', 'db')
    if os.path.isdir(data_dir) is False:
        os.makedirs(data_dir)
    port_op = ['--port']
    if port is not None:
        port_op = port_op + port
    else:
        port_op = []
    # check if mongodb is started already.
    if not (subprocess.call('ps -e | grep -w mongod',
                            shell=True) == 1 and try_lock_pidfile() is True):  # means mongodb is already started, so do not start.
        print 'A mongodb service is already started. You might try to kill the service or run \'./mongod.py stop\'.'
        return 1
    try:
        si = file("/dev/null", 'r')
        so = file("/dev/null", 'a+')
        se = file("/dev/null", 'a+', 0)
        os.dup2(si.fileno(), sys.stdin.fileno())
        p = subprocess.Popen([bin_path, '--dbpath=' + data_dir] + port_op)
        pidfile = write_pidfile(p.pid)
        print "MONGODB PID: %d STARTED. PORT: %s" % (p.pid, (port if port is not None else '27017'))
        print "You can use \'ps -e | grep mongod\' to view the pid of mongodb service."
        os.dup2(so.fileno(), sys.stdout.fileno())
        os.dup2(se.fileno(), sys.stderr.fileno())
        p.communicate()
        pidfile.close()
    except subprocess.CalledProcessError:
        print 'Fail to start mongod service.'
        return 3
    except IOError:
        pidfile.close()
        return 4
    return 0


def try_lock_pidfile():
    # if pidfile is not locked or not existed, return true
    if os.path.isfile(pidfile_path) is False:
        return True
    try:
        with open(pidfile_path, 'r+') as file:
            fcntl.lockf(file, fcntl.LOCK_EX | fcntl.LOCK_NB)
    except IOError:
        return False
    return True


def write_pidfile(pid):
    # write and lock file
    with open(pidfile_path, 'w') as file:
        file.write(str(pid))
    file = open(pidfile_path, 'r+')
    # offer a readwrite exclusive lock so that when stopping the service we can read the content of pidfile.
    fcntl.lockf(file, fcntl.LOCK_EX | fcntl.LOCK_NB)
    return file


def read_pidfile():
    with open(pidfile_path, 'r') as file:
        tmp = file.readline()
        if tmp.endswith(os.linesep):
            tmp = tmp.strip(os.linesep)
        pid = int(tmp)
    return pid


def create_daemon(bin_path, data_dir, port):
    # start mongodb at backend.
    try:
        pid = os.fork()
        if pid > 0: return 0
    except OSError, error:
        print 'fork failed: %d (%s)' % (error.errno, error.strerror)
        os._exit(1)
    os.chdir('/')
    os.setsid()
    os.umask(0)
    sys.stdout.flush()
    sys.stderr.flush()
    return start_mongodb(bin_path, data_dir, port)


def stop_service():
    try:
        if os.path.isfile(pidfile_path):
            with open(pidfile_path, 'r+') as file:
                fcntl.lockf(file, fcntl.LOCK_EX | fcntl.LOCK_NB)
        else:
            pass
    except IOError:
        os.kill(read_pidfile(), signal.SIGTERM)
    finally:
        return 0


def parse_args(args):
    parser = argparse.ArgumentParser()
    parser.add_argument('run', choices=['start', 'stop'])
    parser.add_argument('--data-directory', type=str, help='The directory where database stores data.')
    parser.add_argument('--port', type=str, help='The port of mongodb server.')
    args = parser.parse_args(args)
    if args.run == 'start':
        if args.__contains__('data_directory') is False:
            print "Please specify te data directory of mongodb. "
            raise Exception
        return args.data_directory, args.port
    else:
        sys.exit(stop_service())


def main():
    global pidfile_path
    pidfile_path = os.path.join(os.path.abspath(sys.path[0]), 'etc', 'db.pid')
    if not os.path.isdir(os.path.dirname(pidfile_path)):
        os.makedirs(os.path.dirname(pidfile_path))
    MONGO_DIR = os.path.join(os.path.abspath(sys.path[0]), os.pardir, 'mongodb-server')
    mongodb_path = os.path.join(MONGO_DIR, 'bin', 'mongod')
    try:
        data_dir, port = parse_args(sys.argv[1:])
    except:
        sys.exit(1)
    return create_daemon(mongodb_path, data_dir, port)


if __name__ == '__main__':
    main()
