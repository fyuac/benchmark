# This is a requirement list of Python modules needed by pp-capture.
# This file should be maintained by developers which means when a module is added or no more be used,
# developers should modify this file correspondly otherwise Installer will not pack all correct modules.
# More importantly, the version number after module name should be accurate and can be 
# downloaded by pip(e.g. 'pip download modulename==versionnumber' should download that module successfully).

django-indexer == 0.3.0

futures == 3.0.5

networkx == 1.11

redis == 2.10.5

simplejson == 3.10.0

utils == 0.9.0 

pyzmq == 16.0.0

termcolor == 1.1.0
