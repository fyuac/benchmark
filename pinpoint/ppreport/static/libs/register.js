/**
 * Created by yu on 17/1/2017.
 */

var username_input = $("#username_input");
var passwd_input = $("#passwd_input");
var confirm_passwd_input = $("#confirm_passwd_input");
var root_passwd_input = $("#root_passwd_input");
var register_form = $("#register_form");

var username_div = $("#username_div");
var passwd_div = $("#passwd_div");
var confirm_passwd_div = $("#confirm_passwd_div");
var root_passwd_div = $("#root_passwd_div");

var username_valid = false;
var passwd_valid = false;
var confirm_passwd_valid = false;
var root_passwd_valid = false;

var popover_settings = {
    container: "body",
    html: true,
    trigger: "manual"
};

popover_settings["content"] = "<span class='text-danger'>4-16 chars</span>";
username_input.popover(popover_settings);

popover_settings["content"] = "<span class='text-danger'>At least 6 chars</span>";
passwd_input.popover(popover_settings);

popover_settings["content"] = "<span class='text-danger'>The passwords don't match</span>";
confirm_passwd_input.popover(popover_settings);

popover_settings["content"] = "<span class='text-danger'>Please enter root password</span>";
root_passwd_input.popover(popover_settings);

username_input.on("blur", validate_username);

passwd_input.on("blur", function() {
    validate_passwd();
    if (confirm_passwd_input.val()) {
        validate_confirm_passwd();
    }
});

confirm_passwd_input.on("blur", validate_confirm_passwd);

root_passwd_input.on("blur", validate_root_passwd);

register_form.on("submit", function(event) {
    validate_username();
    validate_passwd();
    validate_confirm_passwd();
    validate_root_passwd();

    if (!(username_valid
        && passwd_valid
        && confirm_passwd_valid
        && root_passwd_valid)) {
        event.preventDefault();
        event.stopPropagation();
    }
});

function validate_username() {
    var name_len = username_input.val().length;
    if (name_len >= 4 && name_len <= 16) {
        username_valid = true;
        username_div.removeClass("has-error");
        username_input.popover("hide");
    } else {
        username_valid = false;
        username_div.addClass("has-error");
        username_input.popover("show");
    }
}

function validate_passwd() {
    var passwd = passwd_input.val();
    if (passwd.length >= 6) {
        passwd_valid = true;
        passwd_div.removeClass("has-error");
        passwd_input.popover("hide");
    } else {
        passwd_valid = false;
        passwd_div.addClass("has-error");
        passwd_input.popover("show");
    }
}

function validate_confirm_passwd() {
    var confirm_passwd = confirm_passwd_input.val();
    var passwd = passwd_input.val();
    if (confirm_passwd === passwd) {
        confirm_passwd_valid = true;
        confirm_passwd_div.removeClass("has-error");
        confirm_passwd_input.popover("hide");
    } else {
        confirm_passwd_valid = false;
        confirm_passwd_div.addClass("has-error");
        confirm_passwd_input.popover("show");
    }
}

function validate_root_passwd() {
    var root_passwd = root_passwd_input.val();
    if (root_passwd.length > 0) {
        root_passwd_valid = true;
        root_passwd_div.removeClass("has-error");
        root_passwd_input.popover("hide");
    } else {
        root_passwd_valid = false;
        root_passwd_div.addClass("has-error");
        root_passwd_input.popover("show");
    }
}
