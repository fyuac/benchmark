/**
 * Setup the bug view main panel, including the bug summary table, code browser, bug trace list.
 *
 * Created by richardxx on 6/26/15.
 */

// \p proj_name: the project name
// \p brf_name: the bug report file name
// \p summary_table: the bug reports selection table
// \p trace_window: a vertical window to display the bug trace
// \p source_manager: manage the source code loading and rendering
function BugViewPanel(proj_name, modl_name, brf_name,
                      summary_table, trace_window, source_manager,
                      is_collapse_cluster, is_show_dbginfo)
{
    // The information shown in the bug reports table
    var bug_report_summary = null;

    // Track the last bug trace and step viewed by the user
    var last_visit_br = -1, last_visit_step = -1;

    var cur_visit_br = -1;

    /*
         Cache the bug traces to reduce communication cost with server.
         Map bug report identity -> ordered list.
     */
    var trace_cache = {};

    // Display settings
    var collapse_cluster = is_collapse_cluster || false;
    var show_dbginfo = is_show_dbginfo || false;

    // Filter settings
    var global_filter_regex = null;
    var global_filter_type = null;
    var filter_type = null;
    var filter_regex = null;

    function filter_summaries(summaries) {
        // If global filter is specified, use global filter to filter the summaries first.
        if (enable_global_filter && global_filter_regex) {
            var filtered_summaries = [];
            for (var i=0; i<summaries.length; i++) {
                var summary = summaries[i];
                var bug_location = summary["FullFile"];
                var reg_test_result = global_filter_regex.test(bug_location);
                if (global_filter_type == "include" && reg_test_result) {
                    filtered_summaries.push(summary);
                } else if (global_filter_type == "exclude" && !reg_test_result) {
                    filtered_summaries.push(summary);
                }
            }
            summaries = filtered_summaries;
        }

        // If filter info is specified, filter the summaries by bug location.
        if (filter_regex) {
            filtered_summaries = [];
            for (i=0; i<summaries.length; i++) {
                summary = summaries[i];
                bug_location = summary["FullFile"];
                reg_test_result = filter_regex.test(bug_location);
                if (filter_type == "include" && reg_test_result) {
                    filtered_summaries.push(summary);
                } else if(filter_type == "exclude" && !reg_test_result) {
                    filtered_summaries.push(summary);
                }
            }
            summaries = filtered_summaries;
        }

        return summaries;
    }

    function fill_bug_reports_table()
    {
        // Get summaries according to collapse_cluster flag
        var summaries = collapse_cluster
            ? bug_report_summary["CollapsedSummary"]
            : bug_report_summary["CompleteSummary"];

        summaries = filter_summaries(summaries);

        bug_descriptions = bug_report_summary["BugDescriptions"];

        // Fill new data to summary table
        var data_table = summary_table.DataTable();
        data_table.clear();
        data_table.rows.add(summaries);
        data_table.draw();

        // If summary table is not empty, add event listener for report clicking
        if (summaries.length > 0) {
            handle_bug_report_click();
        }
    }

    function bug_trace_click_handler()
    {
        var clicked_step = $(this);
        var trace_ol = clicked_step.parent();

        var br_id = trace_ol.data("br_id");
        var step_num = clicked_step.data("step");
        var anchor_line = parseInt(clicked_step.data("anchorline"));
        var file_md5 = clicked_step.data("file_md5");

        if (last_visit_br == br_id && last_visit_step == step_num
            && file_md5 == source_manager.get_cur_file_md5()) {
            // User clicked the trace step that is currently on show
            // Normally this is because user wants to go back to the position of current step in the code window
            source_manager.jump_to_anchor_line(anchor_line);
            return;
        }

        // Highlight the selected trace step
        trace_ol.find("li.selected").removeClass("selected");
        clicked_step.addClass("selected");

        var need_insert_tips = false;

        if (last_visit_br != br_id) {
            source_manager.remove_tips();
            need_insert_tips = true;
        }

        // Load the new file when necessary
        if (source_manager.get_cur_file_md5() != file_md5) {
            var file_id = clicked_step.data("file_id");
            source_manager.update_code(brf_name, file_id, file_md5);
            need_insert_tips = false;
        }

        // Check if we need to change the tips currently in the code_window
        if (need_insert_tips) {
            source_manager.insert_tips(trace_ol, show_dbginfo);
        }

        source_manager.jump_to_anchor_line(anchor_line);

        last_visit_br = br_id;
        last_visit_step = step_num;
    }

    function get_bug_trace_from_server(br_id)
    {
        var trace_ol = null;

        var query_url = "/trace/" + brf_name + "/" + br_id;

        $.ajax({
            type: "GET",
            url: query_url,
            async: false,
            dataType: "json",
            success: function (trace) {
                if (trace.hasOwnProperty("login_required")) {
                    var next = window.location.hash;
                    window.location.replace("/login?next=" + next);
                    throw "";
                }

                trace_ol = $("<ol />",
                    {"data-br_id": br_id}
                );

                var bug_steps = trace["trace"];
                for (var i = 0; i < bug_steps.length; ++i) {
                    var step_summary = bug_steps[i];

                    // Add a list item
                    var li = $("<li />");
                    trace_ol.append(li);

                    // List text
                    var step_info = "<p>" + step_summary["FileName"] + "<br>";
                    step_info += "Line: " + step_summary["Line"] + "</p>";
                    li.html(step_info);

                    // List mouse-over hint
                    li.attr("title", step_summary["FileName"].replace(/\\/g,'/').replace(/.*\//, ''));

                    // List metadata
                    // must be lowercase?
                    li.attr("data-step", (i + 1).toString());
                    li.attr("data-file_id", step_summary["FileID"]);
                    li.attr("data-file_md5", step_summary["FileMD5"]);
                    li.attr("data-anchorline", step_summary["Line"]);

                    // Synthesize inline tip
                    var user_read_tip = step_summary["Tip"];
                    var debug_tip = "";

                    if (step_summary.hasOwnProperty("Inst"))
                        debug_tip += "IR: " + step_summary["Inst"];

                    if (step_summary.hasOwnProperty("Function"))
                        debug_tip += "\n\nFunction: " + step_summary["Function"];

                    li.attr("data-tip", user_read_tip);
                    li.attr("data-debug_tip", debug_tip)
                }
            }
        });

        return trace_ol;
    }

    function handle_bug_report_click()
    {
        summary_table.on("click", "tbody tr", function () {
            // Get the clicked bug report ID
            var tr = $(this);
            var br_id = tr.data("br_id");
            cur_visit_br = br_id;

            // Reset the user tracing data
            last_visit_br = -1;
            last_visit_step = -1;

            // Clean the trace list and the previous trace steps inserted into current source file

            var trace_ol = trace_window.find("ol");
            trace_ol.remove();

            // Get bug trace details for bug report \p br_id
            if (trace_cache.hasOwnProperty(br_id)) {
                trace_ol = trace_cache[br_id];
            }
            else {
                trace_ol = get_bug_trace_from_server(br_id);
                trace_cache[br_id] = trace_ol;
            }
            source_manager.set_trace_ol(trace_ol);
            source_manager.clear_history();

            trace_window.append(trace_ol);

            // Update URL
            if (ppr_type == PPR_ONLINE) {
                window.location.hash = encodeURIComponent(proj_name)
                    + "?" + encodeURIComponent(modl_name)
                    + "?" + brf_name
                    + "?" + br_id;
            }

            // Install the bug trace click event handler
            var trace_steps = trace_ol.children("li");
            trace_steps.on("click", bug_trace_click_handler);

            // Display the first step
            setTimeout(function() {
                trace_steps.first().triggerHandler("click");
            }, 0);

            // Update the bug report view metadata
            $("tr.onview").removeClass("onview");
            tr.addClass("onview");

            // Set current bug report as viewed
            tr.addClass("viewed");

            // Mark the viewed report in bug report summary
            var collapsed_summary = bug_report_summary["CollapsedSummary"];
            for (var i=0; i<collapsed_summary.length; i++) {
                if (collapsed_summary[i]["Identity"] == br_id) {
                    collapsed_summary[i]["Viewed"] = true;
                }
            }
            var complete_summary = bug_report_summary["CompleteSummary"];
            for (i=0; i<complete_summary.length; i++) {
                if (complete_summary[i]["Identity"] == br_id) {
                    complete_summary[i]["Viewed"] = true;
                }
            }

            if (ppr_type == PPR_ONLINE && bottom_grid.is(":visible")) {
                report_detail_panel.load_report_comments(brf_name, tr.data("br_id"));
            }
        });

        // Only add context menu handler to summary_table for online version.
        if (ppr_type == PPR_ONLINE) {
            summary_table.on("contextmenu", "tbody tr", function (e) {
                // return native menu if pressing control
                if (e.ctrlKey) return;
                var tr = $(this);

                //open menu
                context_menu
                    .show()
                    .css({
                        left: getMenuPosition(e.clientX, 'width', 'scrollLeft'),
                        top: getMenuPosition(e.clientY, 'height', 'scrollTop')
                    })
                    .off('click')
                    .on('click', 'a', function () {
                        tr.trigger("click");

                        if (bottom_grid.is(":hidden")) {
                            tool_br_detail.triggerHandler("click");
                        }
                        report_detail_panel.init_add_comment();

                    });
                return false;
            });

            function getMenuPosition(mouse, direction, scrollDir) {
                var win = $(window)[direction](),
                    scroll = $(window)[scrollDir](),
                    menu = context_menu[direction](),
                    position = mouse + scroll;

                // opening menu would pass the side of the page
                if (mouse + menu > win && menu < mouse)
                    position -= menu;

                return position;
            }
        }
    }

    // Retrieve the bug report view from server or from external bug report summary
    function init_view(rep_summary, filter_regex, filter_type)
    {
        source_manager.set_show_dbginfo(show_dbginfo);
        bug_report_summary = null;
        var errmsg = "OK";

        if (typeof rep_summary === "undefined") {
            // Request data from server
            var query_url = "/summary/" + brf_name;

            $.ajax({
                type: "GET",
                url: query_url,
                async: false,
                dataType: "json",
                success: function (summary_json) {
                    if (summary_json.hasOwnProperty("login_required")) {
                        var next = window.location.hash;
                        window.location.replace("/login?next=" + next);
                        throw "";
                    }

                    errmsg = summary_json["errmsg"];
                    if (errmsg != "OK") {
                        return;
                    }

                    if (summary_json["filter_regex"]) {
                        global_filter_regex = new RegExp(summary_json["filter_regex"]);
                        global_filter_type = summary_json["filter_type"];
                    }
                    bug_report_summary = summary_json["summary"];
                },
                error: function () {
                    errmsg = "Cannot connect to server";
                }
            });
        }
        else {
            if (filter_regex) {
                global_filter_regex = new RegExp(filter_regex);
                global_filter_type = filter_type;
            }
            bug_report_summary = rep_summary;
        }

        if (errmsg != "OK")
            console.log(errmsg);

        return errmsg == "OK";
    }

    function install_view()
    {
        uninstall_view();
        fill_bug_reports_table();
    }

    function uninstall_view(__proj_if, __rep_if)
    {
        if (typeof __proj_if !== "undefined" && __proj_if != proj_name)
            return;

        if (typeof __rep_if !== "undefined" && __rep_if != brf_name)
            return;

        // remove summary_table handler
        summary_table.off();

        // clear table contents
        summary_table.DataTable()
            .clear()
            .draw();

        // Delete trace list
        var trace_ol = trace_window.find("ol");
        trace_ol.remove();

        // Show banner text
        source_manager.show_banner();
    }

    function trigger_br(auto_expand, br_id, view_step) {
        if (br_id < 0)
            return;

        var summary_dataTable = summary_table.DataTable();
        var index = summary_dataTable.column(0).data().indexOf(br_id);
        if (auto_expand === true && collapse_cluster && index < 0) {
            collapse_cluster = !collapse_cluster;
            install_view();
            index = summary_dataTable.column(0).data().indexOf(br_id);
        }
        if (index >= 0) {
            // Jump to the page containing the row
            var page_length = summary_dataTable.page.info().length;
            var page_num = Math.floor(index / page_length);
            summary_dataTable.page(page_num).draw(false);

            // Automatically scroll to and show the previous bug report that is under view
            var scrollBody = summary_table.parent();
            // Get the real index in DataTable
            var real_index = summary_dataTable.rows().eq(0)[index];
            var under_view_tr = $(summary_dataTable.row(real_index).node());
            var y_pos = under_view_tr.offset().top - scrollBody.height();
            scrollBody.animate({
                scrollTop: y_pos + scrollBody.scrollTop()
            });

            // Use setTimeout to trigger the click to tr asynchronously
            setTimeout(function() {
                under_view_tr.trigger("click");

                // Show the given trace step
                if (view_step > 0) {
                    var trace_ol = trace_window.find("ol");
                    var trace_step = trace_ol.children("li:nth-child(" + view_step + ")");
                    setTimeout(function() {
                        trace_step.triggerHandler("click");
                    }, 0);
                }
            }, 0);
        }
    }

    function refresh_view() {
        // Install new view
        install_view();
        // Display the previous under view bug report
        trigger_br(false, last_visit_br, last_visit_step);
    }

    function switch_cluster_display()
    {
        collapse_cluster = !collapse_cluster;
        refresh_view();
    }

    function switch_debuginfo_display()
    {
        show_dbginfo = !show_dbginfo;
        source_manager.set_show_dbginfo(show_dbginfo);

        // Insert the tips again in the source code under review
        last_visit_br = -1;
        last_visit_step = -1;
        var trace_ol = trace_window.find("ol");
        var trace_step = trace_ol.find("li.selected");
        setTimeout(function() {
            trace_step.triggerHandler("click");
        }, 0);
    }

    function filter_reports_by_location(type, location_regex) {
        filter_type = type;
        filter_regex = location_regex;
        refresh_view();
    }

    function get_filter_info() {
        return {
            "filter_type": filter_type,
            "filter_regex_txt": filter_regex ? filter_regex.source : null
        }
    }

    /**
     * @param complete: whether get the complete summary.
     * @returns If the param complete is true, return complete summary,
     *          otherwise return the summary according to collapse_cluster flag.
     */
    function get_bug_report_summary(complete) {
        var summaries;
        if (complete || !collapse_cluster) {
            summaries = bug_report_summary["CompleteSummary"];
        } else {
            summaries = bug_report_summary["CollapsedSummary"];
        }

        return filter_summaries(summaries);
    }

    return {
        "init_view": init_view,
        "install_view": install_view,
        "refresh_view": refresh_view,
        "uninstall_view": uninstall_view,
        "switch_cluster_display": switch_cluster_display,
        "switch_debuginfo_display": switch_debuginfo_display,
        "get_bug_report_summary": get_bug_report_summary,
        "trigger_br" : trigger_br,
        "filter_reports_by_location": filter_reports_by_location,
        "get_filter_info": get_filter_info,
        "get_brf_id": function() { return brf_name; },
        "get_onview_br_id": function() { return cur_visit_br; },
        "clear_visit_info": function() {
            cur_visit_br = -1;
            last_visit_br = -1;
            last_visit_step = -1;
        }
    }
}
