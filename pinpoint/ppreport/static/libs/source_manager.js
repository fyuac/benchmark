/**
 * Created by xiao on 6/21/15.
 *
 * Must load jquery before this file.
 */

/*
    We use MD5 value to identify source code globally: MD5 -> Code.
    This way all SourceManager instances can share source code.
    Code is stored as an array, where Code[i] is the i^th line.
 */
var _glb_source_code;
_glb_source_code = {};

var _glb_invalid_MD5 = "~!@#$%^&*";     // Just a random invalid MD5 value

// The buffer size for forward/backward cross reference buffer size
var CROSS_REF_BUF_SIZE = 30;

// The source manager is for managing source code and the symbol cross reference
// \p code_window is the jQuery object for the DOM element that displays code
// \p code_renderer is an instance of code mirror (not a JQuery object)
function SourceManager(code_window, code_renderer)
{
    // cur_file_md5 is the md5 of current displayed file, which could be a file not in the bug report srcFiles.
    var cur_file_md5 = _glb_invalid_MD5;
    var cur_brf;
    var trace_ol;
    var show_dbginfo;
    var symbol_marker = null;
    var highlighted_sym_id = -1;

    var history_positions = [];
    var cur_pos_index = -1;

    // Array of line widgets, used to store the information of inserted tips
    // Data format: {line: line number; widget: lineWidget object of CodeMirror}
    var line_widgets = [];

    // Request source code from server if the code is not available
    function _request_code(brf_name, file_id, file_md5)
    {
        var src_data = null;

        if (_glb_source_code.hasOwnProperty(file_md5))
            src_data = _glb_source_code[file_md5];

        if (src_data == null) {
            var query_url = "/source/" + brf_name + "/" + file_id;
            $.ajax({
                type: "GET",
                url: query_url,
                async: false,
                dataType: "json",
                success: function (data) {
                    if (data.hasOwnProperty("login_required")) {
                        var next = window.location.hash;
                        window.location.replace("/login?next=" + next);
                        throw "";
                    }

                    // Including source code and symbol reference information
                    src_data = data;
                }
            });

            // Run sanity check for the source code information
            if (!src_data.hasOwnProperty("Source")) {
                src_data["Source"] = "Something wrong at server side. Please check.";
                src_data["FileMD5"] = file_md5;
            }
            else if (file_md5 != _glb_invalid_MD5)
                // We do not cache the source for this file_id if its md5_id is not registered
                _glb_source_code[file_md5] = src_data;
        }

        return src_data;
    }

    function _render_ref_symbols(references) {
        if (typeof references === "undefined") {
            return;
        }

        for (var i = 0, len = references.length; i < len; i++) {
            var ref = references[i];
            var line_num = parseInt(ref["Line"]) - 1;
            var start_column = parseInt(ref["StartColumn"]) - 1;
            var end_column = parseInt(ref["EndColumn"]) - 1;

            code_renderer.markText({
                line: line_num,
                ch: start_column
            }, {
                line: line_num,
                ch: end_column
            }, {
                className: "entity " + ref["Symbol"] + " " + ref["RefType"]
            });
        }
    }

    // Update the source code in the code_window with the source file \p file_id
    function update_code(brf_name, file_id, file_md5)
    {
        cur_brf = brf_name;
        var src_data = _request_code(brf_name, file_id, file_md5);
        _render_code(src_data);
    }

    function _load_history_code(file_md5) {
        var src_data = _glb_source_code[file_md5];
        _render_code(src_data);
    }

    function jump_to_anchor_line(anchor_line)
    {
        var cm_line = anchor_line - 1;
        _jump_to_cm_line(cm_line);
        var cursor = code_renderer.getCursor();
        if (cursor["line"] != cm_line) {
            code_renderer.setCursor(cm_line);
        }
        _record_history();
        code_renderer.focus();
    }

    function _jump_to_cm_line(cm_line) {
        var myHeight = code_renderer.getScrollInfo().clientHeight;
        var coords = code_renderer.charCoords({line: cm_line, ch: 0}, "local");
        code_renderer.scrollTo(null, (coords.top + coords.bottom - myHeight) / 2);
    }

    // Insert tips into current file
    function insert_tips(trace_ol, is_show_dbginfo)
    {
        var steps_in_cur_file = trace_ol.find("[data-file_md5='" + cur_file_md5 + "']");

        steps_in_cur_file.each(function() {
            var cur_step = $(this);

            var anchor_line = parseInt(cur_step.data("anchorline")) - 1;
            if (anchor_line < 0) {
                anchor_line = 0;
            }

            // Construct the tip panel
            var step_num = cur_step.attr("data-step");
            var tip = cur_step.attr("data-tip");

            if (is_show_dbginfo) {
                var debug_tip = cur_step.data("debug_tip");
                if (debug_tip != "")
                    tip += "\n\n" + debug_tip;
            }

            var tip_div = $('<div />', {
                'class': "trace_tip",
                'data-step': step_num
            });

            var tip_html = '<span class="badge" style="font-size: 18px">' +
                                step_num + '</span> ' + tip.replace(/\n/g, "<br>");
            tip_div.html(tip_html);

            //var tip_container = $('<div />' , {
            //    'class': "tip_container"
            //});
            //tip_container.append(tip_div);

            var widget = code_renderer.addLineWidget(anchor_line, tip_div.get(0), {
                above: true
            });
            line_widgets.push({line: anchor_line, widget: widget});

            // Highlight corresponding error line
            code_renderer.addLineClass(anchor_line, "text", "trace_line");
        });
    }

    // Remove all tips through clearing all line widgets
    function remove_tips()
    {
        for (var i = 0, len = line_widgets.length; i < len; i++) {
            var lw = line_widgets[i];
            var line = lw["line"];
            var widget = lw["widget"];
            widget.clear();
            code_renderer.removeLineClass(line, "text", "trace_line");
        }
        line_widgets = [];
    }

    function show_banner()
    {
        var banner_code =
            'int main()\n' +
            '{\n' +
            '\tprintf( "Pinpoint is a revolutionary technology to upgrade software industry." );\n' +
            '\tprintf( "We target finding sophisticated inter-components bugs automatically." );\n' +
            '\tprintf( "Our mission is giving programmers more time to their families, not the BUGs." );\n' +
            '\tprintf( "----------------------------------------------------------------------------" );\n' +
            '\tprintf( "All rights reserved by Prism Lab, HKUST." );\n' +
            '}\n';

        code_renderer.setValue(banner_code);

        cur_file_md5 = _glb_invalid_MD5;
        return 0;
    }

    // Obtain the MD5 value for the source code file currently on display
    function get_cur_file_md5() {
        return cur_file_md5;
    }

    function _highlight_symbols(symbolId) {
        var selector = "." + symbolId + ":not(.highlightedSymbol)";
        var symbols = $(selector);
        symbols.addClass("highlightedSymbol");
    }

    function _clear_highlight_symbols() {
        highlighted_sym_id = -1;
        $(".highlightedSymbol").removeClass("highlightedSymbol");
    }

    function _clear_symbol_marker() {
        if (symbol_marker !== null) {
            symbol_marker.clear();
            symbol_marker = null;
        }
    }

    function _mark_symbol(symbol) {
        var line_num = parseInt(symbol["Line"]) - 1;
        var start_column = parseInt(symbol["StartColumn"]) - 1;
        var end_column = parseInt(symbol["EndColumn"]) - 1;

        symbol_marker = code_renderer.markText({
            line: line_num,
            ch: start_column
        }, {
            line: line_num,
            ch: end_column
        }, {
            className: "highlightedSymbol"
        });

        code_renderer.setCursor({line: line_num, ch: end_column});
        _jump_to_cm_line(line_num);
    }

    function _render_code(src_data) {
        remove_tips();

        cur_file_md5 = src_data["FileMD5"];
        var src_code = src_data["Source"];

        var ref_sym_list = null;
        if (src_data.hasOwnProperty("Reference"))
            ref_sym_list = src_data["Reference"];

        if (!_glb_source_code.hasOwnProperty(cur_file_md5)) {
            _glb_source_code[cur_file_md5] = {
                Source: src_code,
                Reference: ref_sym_list
            };
        }

        code_renderer.setValue(src_code);
        code_renderer.focus();

        if (ref_sym_list)
            _render_ref_symbols(ref_sym_list);

        insert_tips(trace_ol, show_dbginfo);
    }

    function _jump_to_history_position() {
        var hist_pos = history_positions[cur_pos_index];

        if (hist_pos["FileMD5"] != cur_file_md5) {
            _load_history_code(hist_pos["FileMD5"]);
        }

        code_renderer.setCursor({line: hist_pos["line"], ch: hist_pos["ch"]});
        _jump_to_cm_line(hist_pos["line"]);
    }

    function _record_history() {
        var cursor = code_renderer.getCursor();
        if (cursor["line"]==0 && cursor["ch"]==0) {
            return;
        }

        if (cur_pos_index >= 0) {
            var last_position = history_positions[cur_pos_index];
            if (last_position["FileMD5"] == cur_file_md5
                && last_position["line"] == cursor["line"]) {
                return;
            }
        }

        var cur_position = {
            FileMD5: cur_file_md5,
            line: cursor["line"],
            ch: cursor["ch"]
        };

        if (cur_pos_index == CROSS_REF_BUF_SIZE) {
            history_positions.shift();
            history_positions.push(cur_position);
        } else {
            history_positions.splice(cur_pos_index + 1, CROSS_REF_BUF_SIZE - cur_pos_index);
            history_positions.push(cur_position);
            cur_pos_index++;
        }
    }

    function setup_cross_reference() {
        code_renderer.on("viewportChange", function() {
            if (highlighted_sym_id != -1) {
                _highlight_symbols(highlighted_sym_id);
            }
        });

        code_window.on("click", ".entity", function (event) {
            var hoverTarget = event.target;
            var classList = hoverTarget.classList;
            var symbolId = -1;
            var refType = -1;
            for (var i=0, len = classList.length; i<len; i++) {
                if (classList[i] == "entity") {
                    // Attach information to class name?
                    symbolId = classList[i+1];
                    refType = classList[i+2];
                    break;
                }
            }

            if (event.ctrlKey || event.metaKey) {
                _record_history();
                //var request_json = {brf_id: cur_brf, symbol_id: symbolId,
                //    ref_type: refType, file_md5: cur_file_md5};
                var query_url = "/reference/" + cur_brf
                    + "/" + symbolId
                    + "/" + refType
                    + "/" + _encode_file_md5(cur_file_md5);
                $.ajax({
                    type: "GET",
                    url: query_url,
                    dataType: "json",
                    async: false,
                    success: function (data) {
                        if (data.hasOwnProperty("login_required")) {
                            var next = window.location.hash;
                            window.location.replace("/login?next=" + next);
                            throw "";
                        }

                        if (data.hasOwnProperty("errmsg")) {
                            console.log(data["errmsg"]);
                            return;
                        }

                        if (data["Index"] === undefined) {
                            // The symbol represents a source code file
                            // Just show that file
                            _render_code(data);
                            code_renderer.setCursor({line: 0, ch: Infinity});
                        } else {
                            if (data["Source"] !== undefined) {
                                // The symbol is not defined in the source code of current file
                                _render_code(data);
                            }
                            _mark_symbol(data["Index"]);
                        }
                        _record_history();
                    }
                });
            } else {
                highlighted_sym_id = symbolId;
                _highlight_symbols(symbolId);
            }
        });

        code_renderer.on("mousedown", function () {
            _clear_highlight_symbols();
            _clear_symbol_marker();
        });

        code_renderer.addKeyMap({
            "Alt-Left": function() {
                // When handling key events, set suppressEdits be false
                // to allow us to change the value in a read-only codemirror.
                code_renderer.state.suppressEdits = false;
                _prev_position();
            }
        });

        code_renderer.addKeyMap({
            "Alt-Right": function() {
                code_renderer.state.suppressEdits = false;
                _next_position();
            }
        });

        //$("#prev").on("click", _prev_position);
        //
        //$("#next").on("click", _next_position);
    }

    function _encode_file_md5(file_md5) {
        file_md5 = file_md5.replace(/@/g, '@at');
        file_md5 = file_md5.replace(/\//g, '@slash');
        return file_md5;
    }

    function _prev_position() {
        if (cur_pos_index > 0) {
            cur_pos_index--;
            _jump_to_history_position();
        }
    }

    function _next_position() {
        if (cur_pos_index < history_positions.length - 1) {
            cur_pos_index++;
            _jump_to_history_position();
        }
    }

    function get_trace_ol() {
        return trace_ol;
    }

    function set_trace_ol(bugview_trace_ol){
        trace_ol = bugview_trace_ol;
    }

    function is_show_dbginfo() {
        return show_dbginfo;
    }

    function set_show_dbginfo(is_show_dbginfo) {
        show_dbginfo = is_show_dbginfo;
    }

    function clear_history() {
        history_positions = [];
        cur_pos_index = -1;
    }

    function init() {
        show_banner();
        setup_cross_reference();
    }

    return {
        "update_code": update_code,
        "insert_tips": insert_tips,
        "remove_tips": remove_tips,
        "jump_to_anchor_line": jump_to_anchor_line,
        "get_cur_file_md5": get_cur_file_md5,
        "get_trace_ol": get_trace_ol,
        "set_trace_ol": set_trace_ol,
        "is_show_dbginfo": is_show_dbginfo,
        "set_show_dbginfo": set_show_dbginfo,
        "clear_history": clear_history,
        "show_banner": show_banner,
        "init": init
    };
}
