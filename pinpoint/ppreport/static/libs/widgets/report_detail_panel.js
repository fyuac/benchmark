/**
 * Javascript for report detail panel.
 *
 * Created by yu on 11/24/16.
 */

function ReportDetailPanel() {
    var cur_brf_id = null;
    var cur_br_id = null;

    var report_detail_panel = $("#report_detail_panel");
    var comments_header = report_detail_panel.find("#comments_header");
    var comments_content = report_detail_panel.find("#comments_content");
    var comments_ol = report_detail_panel.find("#comments_ol");
    var add_comment_btn = report_detail_panel.find("#add_comment_btn");
    var cancel_comment_btn = report_detail_panel.find("#cancel_comment_btn");
    var comment_textarea = report_detail_panel.find("#comment_textarea");
    var comments_list = report_detail_panel.find("#comments_list");

    comment_textarea.on("click", init_add_comment);

    cancel_comment_btn.on("click", exit_add_comment);

    add_comment_btn.on("click", function() {
        var comment_textarea_val = comment_textarea.val().trim();
        if (comment_textarea_val === "") {
            comment_textarea.attr("rows", 1);
            add_comment_btn.hide();
            cancel_comment_btn.hide();
            return;
        }

        var query_url = "/comment/add";

        var comment_data = {
            "brf_id":   cur_brf_id,
            "br_id":    cur_br_id,
            "content":  comment_textarea_val
        };

        $.ajax({
            type: "POST",
            url: query_url,
            contentType: 'application/json; charset=UTF-8',
            data: JSON.stringify(comment_data),
            dataType: "json",
            success: function(result_json) {
                if (result_json.hasOwnProperty("login_required")) {
                    var next = window.location.hash;
                    window.location.replace("/login?next=" + next);
                    throw "";
                }

                var errmsg = result_json["errmsg"];
                if (errmsg != "OK") {
                    alertify.alert(errmsg);
                } else {
                    // Clear current comments
                    comments_ol.empty();
                    comment_textarea.val("");
                    exit_add_comment();
                    // Reload the comments of current report
                    _request_comments(cur_brf_id, cur_br_id);
                }
            }
        });
    });

    comments_ol.on("click", "li ul li a", function() {
        var $a = $(this);
        var action_type = $a.data("type");
        var comment_li = $a.parent().parent().parent();
        var comment_id = comment_li.data("comment_id");


        if (action_type == "edit") {
            var comment_p = comment_li.find("p");
            var action_ul = comment_li.find("ul");
            var comment_edit_textarea = $('<textarea class="form-control comment-textarea" rows="4"></textarea>');
            var edit_comment_btn = $('<button class="btn btn-success">Edit</button>');
            var edit_cancel_btn = $('<button class="btn btn-default" style="margin-left: 10px;">Cancel</button>');
            var edit_div = $('<div style="margin-bottom: 10px"></div>');

            comment_edit_textarea.val(comment_p.text());
            edit_div.append(comment_edit_textarea);
            edit_div.append(edit_comment_btn);
            edit_div.append(edit_cancel_btn);
            comment_p.hide();
            action_ul.hide();
            comment_li.append(edit_div);
            comment_edit_textarea.trigger("focus");

            edit_comment_btn.on("click", function() {
                var query_url = "/comment/edit/" + comment_id;
                var comment_edit_val = comment_edit_textarea.val();
                var comment_data = {
                    "content":  comment_edit_val
                };

                $.ajax({
                    type: "POST",
                    url: query_url,
                    contentType: 'application/json; charset=UTF-8',
                    data: JSON.stringify(comment_data),
                    dataType: "json",
                    success: function(result_json) {
                        if (result_json.hasOwnProperty("login_required")) {
                            var next = window.location.hash;
                            window.location.replace("/login?next=" + next);
                            throw "";
                        }

                        var errmsg = result_json["errmsg"];
                        if (errmsg != "OK") {
                            alertify.alert(errmsg);
                        } else {
                            edit_div.remove();
                            comment_p.text(comment_edit_val);
                            comment_p.show();
                            action_ul.show();
                        }
                    }
                });
            });

            edit_cancel_btn.on("click", function() {
                edit_div.remove();
                comment_p.show();
                action_ul.show();
            });

        } else if (action_type == "delete") {
            var dialog_msg = "Are you sure you want to delete this comment?";
            alertify.confirm(dialog_msg, function (e) {
                if (!e) {
                    return;
                }
                var query_url = "/comment/delete/" + comment_id;
                $.ajax({
                    type: "GET",
                    url: query_url,
                    contentType: 'application/json; charset=UTF-8',
                    dataType: "json",
                    success: function(result_json) {
                        if (result_json.hasOwnProperty("login_required")) {
                            var next = window.location.hash;
                            window.location.replace("/login?next=" + next);
                            throw "";
                        }

                        var errmsg = result_json["errmsg"];
                        if (errmsg != "OK") {
                            alertify.alert(errmsg);
                        } else {
                            // Clear current comments
                            comments_ol.empty();
                            // Reload the comments of current report
                            _request_comments(cur_brf_id, cur_br_id);
                        }
                    }
                });
            });
        }
    });

    function init_add_comment() {
        comment_textarea.attr("rows", 4);
        add_comment_btn.show();
        cancel_comment_btn.show();
    }

    function exit_add_comment() {
        comment_textarea.attr("rows", 1);
        add_comment_btn.hide();
        cancel_comment_btn.hide();
    }

    function load_report_comments(brf_id, br_id) {
        cur_brf_id = brf_id;
        cur_br_id = br_id;

        exit_add_comment();
        comments_content.show();
        comments_ol.empty();
        comments_header.text("Comments (0)");
        _request_comments(brf_id, br_id);
    }

    function clear_comments() {
        comments_header.text("Please select a bug report first.");
        comments_content.hide();
        comments_ol.empty();
    }

    function _request_comments(brf_id, br_id) {
        var query_url = "/comment/" + brf_id + "/" + br_id;
        $.ajax({
            type: "GET",
            url: query_url,
            contentType: 'application/json; charset=UTF-8',
            dataType: "json",
            success: function(result_json) {
                if (result_json.hasOwnProperty("login_required")) {
                    var next = window.location.hash;
                    window.location.replace("/login?next=" + next);
                    throw "";
                }

                var comments = result_json["comments"];
                var comments_len = comments.length;
                comments_header.text("Comments (" + comments_len + ")");
                for (var i=0; i<comments_len; i++) {
                    var comm = comments[i];
                    var li = $("<li/>");
                    li.attr("class", "comment");
                    li.data("comment_id", comm["_id"]);
                    li.append("<h5>" + comm["user_name"] + "</h5>");
                    li.append("<p>" + comm["content"] + "</p>");

                    var ul = $("<ul/>");
                    ul.attr("class", "comment-actions");
                    // Only the comments from current user can be edited or deleted.
                    if (comm["user_name"] == user_name) {
                        ul.append("<li><a data-type='edit'>Edit</a></li>");
                        ul.append("<li><a data-type='delete'>Delete</a></li>");
                    }
                    ul.append("<li><a>" + comm["time"] + "</a></li>");

                    li.append(ul);
                    comments_ol.append(li);
                }
            }
        });
    }

    return {
        "load_report_comments": load_report_comments,
        "clear_comments": clear_comments,
        "init_add_comment": init_add_comment
    }
}
