/**
 * Created by yu on 12/12/2016.
 */

function VerifyRootDialog() {
    var verify_root_dialog = $("#verify_root_dialog");
    var root_passwd_input = $("#root_passwd_input");
    var verify_root_btn = $("#verify_root_btn");
    var dfd = null;
    var confirmed = false;

    verify_root_btn.on("click", function() {
        confirmed = true;
        close_dialog();
    });

    verify_root_dialog.on("hide.bs.modal", function() {
        if (confirmed) {
            dfd.resolve(root_passwd_input.val());
        } else {
            dfd.reject();
        }

        confirmed = false;
        root_passwd_input.val("");
    });

    function open_dialog() {
        // Use JQuery Deferred object to pass the root password to a callback function
        // after the root password is inputted and the OK button is clicked.
        dfd = $.Deferred();
        verify_root_dialog.modal("show");
        return dfd.promise();
    }

    function close_dialog() {
        verify_root_dialog.modal("hide");
    }

    return {
        "open_dialog": open_dialog
    }
}
