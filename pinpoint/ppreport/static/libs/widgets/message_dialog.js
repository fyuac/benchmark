/**
 * Created by yu on 12/26/16.
 */

function MessageDialog() {
    var message_dialog = $("#message_dialog");
    var message_title = $("#message_title");
    var message_content = $("#message_content");

    function open_dialog(title, content) {
        if (title)
            set_title(title);
        if (content)
            set_content(content);
        message_dialog.modal("show");
    }

    function set_title(title) {
        message_title.text(title);
    }

    function set_content(content) {
        message_content.html(content);
    }

    return {
        "open_dialog": open_dialog,
        "set_title": set_title,
        "set_content": set_content
    }
}
