/**
 * Created by yu on 9/11/16.
 *
 * The bug reports filtering panel based on bootstrap modal.
 */

function create_filter_dialog() {
    var filter_dialog = $("#filter_dialog");
    var filter_type_radio = $("input:radio[name=filter_type]");
    var filter_regex_input = $("#filter_regex");
    var filter_btn = $("#filter_btn");
    var filter_clear_btn = $("#filter_clear_btn");
    var regex_errmsg = $("#regex_errmsg");

    function open_dialog() {
        filter_dialog.modal("show");
    }

    function close_dialog() {
        filter_dialog.modal("hide");
    }

    filter_dialog.on("show.bs.modal", function() {
        filter_btn.prop('disabled', true);
        regex_errmsg.text("");

        var filter_info = cur_view_panel.get_filter_info();
        filter_type_radio.filter("[value=" + filter_info["filter_type"] + "]").prop("checked");
        filter_regex_input.val(filter_info["filter_regex_txt"]);

        filter_btn.prop('disabled', false);
        filter_clear_btn.prop('disabled', false);
    });

    filter_btn.on("click", function() {
        filter_btn.prop('disabled', true);
        filter_clear_btn.prop('disabled', true);

        var filter_type = filter_type_radio.filter(":checked").val();
        var filter_regex_txt = filter_regex_input.val();

        if (filter_regex_txt) {
            try {
                var filter_regex = new RegExp(filter_regex_txt);
                cur_view_panel.filter_reports_by_location(filter_type, filter_regex);
                close_dialog();
            } catch (e){
                regex_errmsg.text("Invalid regex!");
                filter_btn.prop('disabled', false);
                filter_clear_btn.prop('disabled', false);
            }
        } else {
            close_dialog();
        }
    });

    filter_clear_btn.on("click", function() {
        filter_btn.prop('disabled', true);
        filter_clear_btn.prop('disabled', true);
        cur_view_panel.filter_reports_by_location(null, null);
        close_dialog();
    });

    return {
        "open_dialog": open_dialog
    };
}
