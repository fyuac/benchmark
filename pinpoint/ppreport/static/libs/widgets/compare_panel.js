/**
 * Created by richardxx on 2/28/16.
 *
 * The bug reports comparison panel based on bootstrap,
 * and the comparison config dialog based on bootstrap modal.
 */

function create_compare_reports_widget(proj_selector, rep_selector, dataTable_config, trace_window, source_manager)
{
    "use strict";

    var compare_config_dialog = $("#compare_config_dialog");
    var compare_panel = $("#compare_panel");

    var comp_proj_selector = $("#comp_proj_selector");
    var comp_modl_selector = $("#comp_modl_selector");
    var comp_rep_selector = $("#comp_rep_selector");

    var compare_table1 = $("#compare_table1");
    var compare_table2 = $("#compare_table2");

    var title_bar1 = $("#report_name1");
    var title_bar2 = $("#report_name2");

    var compare_btn = $("#compare_btn");

    function init() {
        compare_table1.DataTable(dataTable_config);
        compare_table2.DataTable(dataTable_config);

        $("#compare_panel_close").click(function() {
            compare_panel.hide();
        });

        compare_config_dialog.on("show.bs.modal", function() {
            load_compare_reports_list();
            compare_btn.prop('disabled', false);
        });

        compare_btn.on("click", compare_reports);

        comp_proj_selector.on("change", update_comp_modules_list);
        comp_modl_selector.on("change", update_comp_reports_list)
    }

    init();

    // Select the compare table scrollBody elements after initiating DataTable widget for compare tables.
    var compare_table1_scrollBody = $("div#compare_table1_wrapper .dataTables_scrollBody");
    var compare_table2_scrollBody = $("div#compare_table2_wrapper .dataTables_scrollBody");

    function load_compare_reports_list() {
        var selected_rep_option = rep_selector.find(":selected");

        // If there is project selector in the dialog,
        // load the project options from the project selector in control panel.
        if (comp_proj_selector.length == 1) {
            var proj_options = proj_selector.find("option:first-child").nextAll();
            comp_proj_selector.find("option:first-child").nextAll().remove();
            comp_proj_selector.append(proj_options.clone());

            var sel_proj_option = proj_selector.find(":selected");
            comp_proj_selector.selectpicker("val", sel_proj_option.val());
            comp_proj_selector.selectpicker("refresh");


            var modl_options = modl_selector.find("option:first-child").nextAll();
            comp_modl_selector.find("option:first-child").nextAll().remove();
            comp_modl_selector.append(modl_options.clone());

            var sel_modl_option = modl_selector.find(":selected");
            comp_modl_selector.selectpicker("val", sel_modl_option.val());
            comp_modl_selector.selectpicker("refresh");
        }

        var rep_options = rep_selector.find("option:first-child").nextAll();
        comp_rep_selector.find("option:first-child").nextAll().remove();
        rep_options.each(function() {
            var option = $(this);
            if (option[0] != selected_rep_option[0]) {
                comp_rep_selector.append(option.clone());
            }
        });

        comp_rep_selector.selectpicker("refresh");
    }

    function update_comp_modules_list() {
        var selected_proj_option = comp_proj_selector.find(":selected");
        comp_modl_selector.find("option:first-child").nextAll().remove();
        comp_rep_selector.find("option:first-child").nextAll().remove();

        var sel_proj = selected_proj_option.text();
        var query_url = "/modules";
        var request_data = {
            "proj_name": sel_proj
        };

        $.ajax({
            type: "POST",
            url: query_url,
            contentType: 'application/json; charset=UTF-8',
            async: true,
            data: JSON.stringify(request_data),
            dataType: "json",
            success: function (res_json) {
                if (res_json.hasOwnProperty("login_required")) {
                    var next = window.location.hash;
                    window.location.replace("/login?next=" + next);
                    throw "";
                }

                var errmsg = res_json["errmsg"];
                if (errmsg != "OK") {
                    alertify.alert(errmsg);
                    return;
                }

                var modules = res_json["modl_list"];
                for (var i=0; i<modules.length; i++) {
                    var modl_name = modules[i];
                    var option = $("<option/>").val(modl_name).text(modl_name);
                    comp_modl_selector.append(option);
                }

                comp_modl_selector.selectpicker('refresh');
                comp_rep_selector.selectpicker('refresh');

                // Automatically expand the modules dropdown list
                comp_modl_selector.selectpicker('toggle');
                comp_modl_selector.focus();
            }
        });
    }

    function update_comp_reports_list() {
        var selected_proj_option = comp_proj_selector.find(":selected");
        var selected_modl_option = comp_modl_selector.find(":selected");
        comp_rep_selector.find("option:first-child").nextAll().remove();

        var sel_proj = selected_proj_option.text();
        var sel_modl = selected_modl_option.text();
        var query_url = "/reports";
        var request_data = {
            "proj_name": sel_proj,
            "modl_name": sel_modl
        };

        $.ajax({
            type: "POST",
            url: query_url,
            async: true,
            contentType: 'application/json; charset=UTF-8',
            data: JSON.stringify(request_data),
            dataType: "json",
            success: function (rep_files_json) {
                if (rep_files_json.hasOwnProperty("login_required")) {
                    var next = window.location.hash;
                    window.location.replace("/login?next=" + next);
                    throw "";
                }

                var errmsg = rep_files_json["errmsg"];
                if (errmsg != "OK") {
                    alertify.alert(errmsg);
                    return;
                }

                // rep_files is an array of report files
                // Each item is an object with "name", "id" and "type" 3 fields
                var rep_files = rep_files_json["report_files"];

                var all_option = null;
                var last_option = null;
                var option_array = [];
                for (var i=0; i<rep_files.length; i++) {
                    var brf = rep_files[i];
                    var brf_type = brf["type"];

                    if (brf_type) {
                        if (brf_type == "all")
                            all_option = $("<option />")
                                .val(brf["id"])
                                .text("All Reports")
                                .css("font-weight", "bold")
                                .data("type", brf_type);
                        else if  (brf_type == "last")
                            last_option = $("<option />")
                                .val(brf["id"])
                                .text("Last Uploaded Reports")
                                .css("font-weight", "bold")
                                .data("type", brf_type);
                    } else {
                        var option = $("<option />").val(brf["id"]).text(brf["name"]);
                        option_array.push(option);
                    }
                }

                if (all_option) {
                    option_array.unshift($('<option data-divider="true"></option>'));
                    option_array.unshift(last_option);
                    option_array.unshift(all_option);
                }

                comp_rep_selector.append(option_array);
                comp_rep_selector.selectpicker('refresh');

                // Automatically expand the compared reports dropdown list
                comp_rep_selector.selectpicker('toggle');
                comp_rep_selector.focus();
            }
        });
    }

    function show_compare_reports(sel_comp_rep)
    {
        var sel_proj_name;

        if (typeof proj_selector === "string")
            sel_proj_name = proj_selector;
        else {
            var sel_proj_option = proj_selector.find(":selected");
            sel_proj_name = sel_proj_option.text();
        }

        var sel_modl_option = modl_selector.find(":selected");
        var sel_modl_name = sel_modl_option.text();
        var sel_rep_option = rep_selector.find(":selected");
        var sel_rep_id = sel_rep_option.val();
        var sel_rep_name = sel_rep_option.text();

        var sel_comp_rep_id = sel_comp_rep.val();
        var sel_comp_rep_name = sel_comp_rep.text();

        title_bar1.text(sel_rep_name.split(/[\\/]/).pop());
        title_bar2.text(sel_comp_rep_name.split(/[\\/]/).pop());

        var query_url = "/compare_reports/" + sel_rep_id + "/" + sel_comp_rep_id;

        $.ajax({
            type: "GET",
            url: query_url,
            async: true,
            dataType: "json",
            success: function(data) {
                if (data.hasOwnProperty("login_required")) {
                    var next = window.location.hash;
                    window.location.replace("/login?next=" + next);
                    throw "";
                }

                var errmsg = data["errmsg"];
                if ( errmsg != "OK") {
                    return;
                }

                var filter_regex = data["filter_regex"];
                var filter_type = data["filter_type"];
                var report_summary1 = data["summary1"];
                var report_summary2 = data["summary2"];

                var comp_panel1 = BugViewPanel(sel_proj_name, sel_modl_name, sel_rep_id,
                    compare_table1, trace_window, source_manager, false, true);

                var comp_panel2 = BugViewPanel(sel_proj_name, sel_modl_name, sel_comp_rep_id,
                    compare_table2, trace_window, source_manager, false, true);

                var init_status = comp_panel1.init_view(report_summary1, filter_regex, filter_type);
                init_status = init_status && comp_panel2.init_view(report_summary2, filter_regex, filter_type);

                if (init_status) {
                    comp_panel1.install_view();
                    comp_panel2.install_view();
                }
            },
            error: function() {
                console.log("Cannot connect to server");
            }
        });

        close_config_dialog();
    }

    function compare_reports()
    {
        compare_btn.prop('disabled', true);
        var sel_comp_rep_option = comp_rep_selector.find(":selected");
        if (sel_comp_rep_option.is("#comp_none_rep")) {
            alertify.alert("Please select another report to compare!", undefined, "dialog-font");
            compare_btn.prop('disabled', false);
        } else {
            compare_panel.show();
            show_compare_reports(sel_comp_rep_option);
        }
    }

    function adjust_compare_panel()
    {
        // panel borders height: 2 * 2;
        // panel headers height: 38 * 3;
        // margins height: 3 * 3;
        // the sum of table headers and pagination components heights: 72 * 2;
        // the sum of the heights mentioned above is 271.
        var compare_dataTable_height = (compare_panel.height() - 271) / 2;
        compare_table1_scrollBody.height(compare_dataTable_height);
        compare_table2_scrollBody.height(compare_dataTable_height);
    }

    function close_config_dialog()
    {
        compare_config_dialog.modal("hide");
    }

    function open_config_dialog()
    {
        compare_config_dialog.modal("show");
    }

    return {
        "open_config_dialog": open_config_dialog,
        "adjust_compare_panel": adjust_compare_panel
    };
}
