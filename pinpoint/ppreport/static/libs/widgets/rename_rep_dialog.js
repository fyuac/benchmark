/**
 * Created by yu on 9/28/16.
 *
 * The dialog for renaming bug report, based on bootstrap.
 */

function RenameRepDialog() {
    var rename_dialog = $("#rename_rep_dialog");
    var new_name_input = $("#new_rep_name");
    var rename_btn = $("#rename_rep_btn");
    var rename_errmsg = $("#rename_rep_errmsg");
    var sel_rep_option = null;
    var report_name = "";
    var time_info = "";

    function open_dialog() {
        rename_dialog.modal("show");
    }

    function close_dialog() {
        rename_dialog.modal("hide");
    }

    rename_dialog.on("show.bs.modal", function() {
        sel_rep_option = rep_selector.find(":selected");
        var rep_option_txt = sel_rep_option.text();

        // remove the time info in report name
        var end_idx = rep_option_txt.lastIndexOf('(');
        report_name = rep_option_txt.slice(0, end_idx);
        time_info = rep_option_txt.slice(end_idx);

        new_name_input.val(report_name);
        rename_btn.prop('disabled', false);
    });

    rename_btn.on("click", function() {
        var new_name = new_name_input.val();
        if (new_name == "") {
            rename_errmsg.text("Please input a new name.");
        } else if (new_name == report_name) {
            close_dialog();
        } else {
            rename_btn.prop('disabled', true);
            var report_id = sel_rep_option.val();
            var query_url = "/reports/rename/" + report_id + "/" + new_name;

            $.ajax({
                type: "GET",
                url: query_url,
                async: false,
                dataType: "json",
                success: function (result_json) {
                    if (result_json.hasOwnProperty("login_required")) {
                        var next = window.location.hash;
                        window.location.replace("/login?next=" + next);
                        throw "";
                    }

                    var errmsg = result_json["errmsg"];
                    if (errmsg != "OK") {
                        alertify.alert(errmsg);
                    } else {
                        sel_rep_option.text(new_name + time_info);
                        rep_selector.selectpicker('refresh');
                    }
                }
            });

            close_dialog();
        }
    });

    return {
        "open_dialog": open_dialog
    };
}
