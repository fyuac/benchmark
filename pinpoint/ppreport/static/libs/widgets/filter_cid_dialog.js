/**
 * Created by yu on 12/27/16.
 */

function FilterCidDialog() {
    var filter_cid_dialog = $("#filter_cid_dialog");
    var filter_cid_input = $("#filter_cid_input");
    var filter_cid_errmsg = $("#filter_cid_errmsg");
    var filter_cid_btn = $("#filter_cid_btn");
    var cid = -1;
    var dfd = null;
    var confirmed = false;

    filter_cid_btn.on("click", function() {
        cid = filter_cid_input.val();
        cid = parseInt(cid);
        if (isNaN(cid)) {
            filter_cid_errmsg.addClass("bg-warning");
            filter_cid_errmsg.show();
            setTimeout(function () {
                filter_cid_errmsg.removeClass("bg-warning");
            }, 1000);
        } else {
            confirmed = true;
            close_dialog();
        }
    });

    filter_cid_dialog.on("hide.bs.modal", function() {
        if (confirmed) {
            dfd.resolve(cid);
        } else {
            dfd.reject();
        }

        confirmed = false;
    });

    function open_dialog() {
        filter_cid_errmsg.hide();
        filter_cid_dialog.modal("show");
        dfd = $.Deferred();
        return dfd.promise();
    }

    function close_dialog() {
        filter_cid_dialog.modal("hide");
    }


    return {
        "open_dialog": open_dialog
    }
}