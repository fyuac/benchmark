/**
 * Created by xiao on 7/24/15.
 *
 * The bug reports uploading panel based on jqueryUI.js.
 */

// upload_dialog: the JQuery object for a <div> that is served for dialog UI
function create_upload_dialog()
{
    var upload_dialog = $("#upload_dialog");
    var tip_area = upload_dialog.find(".validateTips");
    var txt_proj_name = $("#txt_proj_name");
    var txt_module = $("#txt_module");
    var txt_rep_name = $("#txt_rep_name");
    var txt_server_addr = $("#txt_server_addr");
    var txt_user_name = $("#txt_user_name");
    var txt_user_passwd = $("#txt_user_passwd");
    var upload_btn = $("#upload_btn");

    var has_uploaded = false;

    function init()
    {
        $.ajax({
            type: "GET",
            url: "/remote_server",
            async: true,
            success: function (remote_info) {
                var server_addr = remote_info["server_addr"];
                var user_name = remote_info["user_name"];
                var user_passwd = remote_info["user_passwd"];
                var keep_file_name = remote_info["keep_file_name"];

                txt_server_addr.val(server_addr);
                txt_user_name.val(user_name);
                txt_user_passwd.val(user_passwd);
            }
        });

        upload_dialog.on("show.bs.modal", function() {
            if (has_uploaded) {
                update_tips("Bug reports have been uploaded once.");
            } else {
                update_tips("", false);
                upload_btn.prop("disabled", false);
            }
        });

        upload_btn.on("click", submit_report);
    }

    init();

    function open_dialog() {
        upload_dialog.modal("show");
    }

    function update_tips(t, highlight)
    {
        if (!t) {
            tip_area.hide();
            return;
        }

        tip_area.show();
        tip_area.text(t);

        if (typeof highlight === "undefined") highlight = true;
        if (highlight) {
            tip_area.addClass("bg-danger");
            setTimeout(function () {
                tip_area.removeClass("bg-danger");
            }, 1000);
        }
    }

    function cons_upload_setting()
    {
        var invalid_input = false;
        var txt_names = ["project name", "server address", "user name", "user password"];
        var upload_setting = {};

        $([txt_proj_name, txt_server_addr, txt_user_name, txt_user_passwd]).each(
            function (index)
            {
                if (invalid_input) return;

                var txt_input = $(this);
                var input_name = txt_input.prop("name");
                var input_value = txt_input.val();
                if (input_value == "") {
                    update_tips("Please specify a " + txt_names[index] + ".");
                    invalid_input = true;
                }

                upload_setting[input_name] = input_value;
            }
        );

        if (invalid_input) return null;

        upload_setting["tag"] = txt_rep_name.val();
        upload_setting["module"] = txt_module.val();

        return upload_setting;
    }

    function submit_report()
    {
        var upload_setting = cons_upload_setting();
        if (upload_setting == null) return;

        upload_btn.prop("disabled", true);

        var timer_tips = null;

        $.ajax({
            type: "POST",
            url: "/upload/",
            contentType: 'application/json; charset=UTF-8',
            data: JSON.stringify(upload_setting),
            dataType: "json",
            async: true,
            beforeSend: function() {
                var n_ellipse = 0;
                timer_tips = setInterval(function() {
                    var tip = "Sending in progress";
                    if (String.repeat != undefined)
                        tip += ".".repeat(n_ellipse + 1);
                    else
                        tip += Array(n_ellipse + 2).join(".");

                    if (++n_ellipse > 5) n_ellipse = 0;
                    update_tips(tip, false);
                }, 500);
            },
            success: function (ret_data) {
                var errmsg = ret_data["errmsg"];
                update_tips(errmsg);

                var state = ret_data["state"];
                if (state == true)
                    has_uploaded = true;
                else
                    upload_btn.prop("disabled", false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                update_tips(errorThrown);
                upload_btn.prop("disabled", false);
            },
            complete: function() {
                if (timer_tips != null) {
                    clearTimeout(timer_tips);
                    timer_tips = null;
                }
            }
        });
    }

    var form = upload_dialog.find("form").on("submit", function (event) {
        event.preventDefault();
        if (has_uploaded)
            return;
        submit_report();
    });

    return {
        "open_dialog": open_dialog
    };
}
