/**
 * Created by yu on 9/12/2016.
 *
 * The dialog for renaming module, based on bootstrap
 */

function RenameModlDialog() {
    var rename_dialog = $("#rename_modl_dialog");
    var new_name_input = $("#new_modl_name");
    var rename_btn = $("#rename_modl_btn");
    var rename_errmsg = $("#rename_modl_errmsg");
    var sel_modl_option = null;
    var modl_name = "";

    function open_dialog() {
        rename_dialog.modal("show");
    }

    function close_dialog() {
        rename_dialog.modal("hide");
    }

    rename_dialog.on("show.bs.modal", function() {
        sel_modl_option = modl_selector.find(":selected");
        modl_name = sel_modl_option.text();

        new_name_input.val(modl_name);
        rename_btn.prop('disabled', false);
    });

    rename_btn.on("click", function () {
        var new_name = new_name_input.val();
        if (new_name == "") {
            rename_errmsg.text("Please input a new name.");
        } else if (new_name == modl_name) {
            close_dialog();
        } else {
            rename_btn.prop('disabled', true);
            var query_url = "/module/rename";
            var proj_name = proj_selector.find(":selected").text();
            var rename_data = {
                "proj_name": proj_name,
                "orig_name": modl_name,
                "new_name": new_name
            };

            $.ajax({
                type: "POST",
                url: query_url,
                contentType: 'application/json; charset=UTF-8',
                data: JSON.stringify(rename_data),
                dataType: "json",
                async: false,
                success: function (result_json) {
                    if (result_json.hasOwnProperty("login_required")) {
                        var next = window.location.hash;
                        window.location.replace("/login?next=" + next);
                        throw "";
                    }

                    var errmsg = result_json["errmsg"];
                    if (errmsg != "OK") {
                        alertify.alert(errmsg);
                    } else {
                        // Update project selector
                        sel_modl_option.text(new_name);
                        modl_selector.selectpicker('refresh');

                        // Update url hash
                        var url_hash = window.location.hash;
                        var trace_locator = url_hash.substr(1).split("?");
                        var proj_name = trace_locator[0];
                        var brf_name = trace_locator[2] || "";
                        var br_id = trace_locator[3] || "";

                        url_hash = proj_name + "?" + encodeURIComponent(new_name);
                        if (brf_name) {
                            url_hash = url_hash + "?" + brf_name;
                            if (br_id)
                                url_hash = url_hash + "?" + br_id;
                        }

                        window.location.hash = url_hash;
                    }
                }
            });

            close_dialog();
        }
    });

    return {
        "open_dialog": open_dialog
    }
}
