/**
 * Created by yu on 12/26/16.
 */

function ConfirmDialog() {
    var confirm_dialog = $("#confirm_dialog");
    var confirm_title = $("#confirm_title");
    var confirm_content = $("#confirm_content");
    var confirm_btn = $("#confirm_btn");
    var dfd = null;
    var confirmed = false;

    confirm_btn.on("click", function() {
        confirmed = true;
        close_dialog();
    });

    confirm_dialog.on("hide.bs.modal", function() {
        if (confirmed) {
            dfd.resolve();
        } else {
            dfd.reject();
        }

        confirmed = false;
    });

    function open_dialog(title, content) {
        if (title)
            set_title(title);
        if (content)
            set_content(content);

        confirm_dialog.modal("show");
        dfd = $.Deferred();
        return dfd.promise();
    }

    function close_dialog() {
        confirm_dialog.modal("hide");
    }

    function set_title(title) {
        confirm_title.text(title);
    }

    function set_content(content) {
        confirm_content.html(content);
    }

    return {
        "open_dialog": open_dialog,
        "set_title": set_title,
        "set_content": set_content
    }
}
