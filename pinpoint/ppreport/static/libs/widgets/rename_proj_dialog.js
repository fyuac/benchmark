/**
 * Created by yu on 10/31/16.
 *
 * The dialog for renaming project, based on bootstrap
 */

function RenameProjDialog() {
    var rename_dialog = $("#rename_proj_dialog");
    var new_name_input = $("#new_proj_name");
    var rename_btn = $("#rename_proj_btn");
    var rename_errmsg = $("#rename_proj_errmsg");
    var sel_proj_option = null;
    var proj_name = "";

    function open_dialog() {
        rename_dialog.modal("show");
    }

    function close_dialog() {
        rename_dialog.modal("hide");
    }

    rename_dialog.on("show.bs.modal", function() {
        sel_proj_option = proj_selector.find(":selected");
        proj_name = sel_proj_option.text();

        new_name_input.val(proj_name);
        rename_btn.prop('disabled', false);
    });

    rename_btn.on("click", function () {
        var new_name = new_name_input.val();
        if (new_name == "") {
            rename_errmsg.text("Please input a new name.");
        } else if (new_name == proj_name) {
            close_dialog();
        } else {
            rename_btn.prop('disabled', true);
            var query_url = "/projects/rename";
            var rename_data = {
                "original_name":    proj_name,
                "new_name":         new_name
            };

            $.ajax({
                type: "POST",
                url: query_url,
                contentType: 'application/json; charset=UTF-8',
                data: JSON.stringify(rename_data),
                dataType: "json",
                async: false,
                success: function (result_json) {
                    if (result_json.hasOwnProperty("login_required")) {
                        var next = window.location.hash;
                        window.location.replace("/login?next=" + next);
                        throw "";
                    }

                    var errmsg = result_json["errmsg"];
                    if (errmsg != "OK") {
                        alertify.alert(errmsg);
                    } else {
                        // Update project selector
                        sel_proj_option.text(new_name);
                        proj_selector.selectpicker('refresh');

                        // Update url hash
                        var url_hash = window.location.hash;
                        var trace_locator = url_hash.substr(1).split("?");
                        var modl_name = trace_locator[1] || "";
                        var brf_name = trace_locator[2] || "";
                        var br_id = trace_locator[3] || "";

                        url_hash = encodeURIComponent(new_name);
                        if (modl_name) {
                            url_hash = url_hash + "?" + modl_name;
                            if (brf_name) {
                                url_hash = url_hash + "?" + brf_name;
                                if (br_id)
                                    url_hash = url_hash + "?" + br_id;
                            }
                        }

                        window.location.hash = url_hash;
                    }
                }
            });

            close_dialog();
        }
    });

    return {
        "open_dialog": open_dialog
    }
}
