#!/bin/bash

BASEDIR=$(dirname $(readlink -f $0))
cd $BASEDIR

wsgi_name="gunicorn"

if [[ $# > 0 ]]; then
    wsgi_name=$1
fi
if [ "$wsgi_name" == "gunicorn" ]; then
    gunicorn -w 1 -b 0.0.0.0:40072 pp_online:flask_server --log-file ppo.log \
    --log-level=debug -D -k gevent --capture-output
elif [ "$wsgi_name" == "uwsgi" ]; then
    uwsgi --ini uwsgi.ini
else
    echo "Start a standalone server"
    ./pp_online.py
fi

