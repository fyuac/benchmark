use PPOService;

db.bug_reports.find().forEach(function(e) {
    if (e["tag"] == "") {
        e["module"] = "unnamed"
    } else {
        e["module"] = e["tag"];
    }

    db.bug_reports.save(e);
});
