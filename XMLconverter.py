import json
import dicttoxml
import xmltodict

def json2xml2(file_path):
    #json_file = file_path + ".json"
    json_data = open(file_path).read()
    data = json.loads(json_data)
    xml_data = dicttoxml.dicttoxml(data)
    new_file_path = file_path[:-5]
    xml_file = new_file_path + ".xml"
    file = open(xml_file, 'wb')
    if xml_data != None:
        file.write(xml_data)
    return xml_file

def json2xml(file_path):
    #json_file = file_path + ".json"
    json_data = open(file_path).read()
    data = json.loads(json_data)
    xml_data = xmltodict.unparse(data)
    new_file_path = file_path[:-5]
    xml_file = new_file_path + ".xml"
    file = open(xml_file, 'w+')
    if xml_data != None:
        file.write(xml_data)
    return  xml_file

def xml2json(file_path):
    #json_file = file_path + ".xml"
    xml_data = open(file_path).read()
    data = xmltodict.parse(xml_data)
    json_data = json.dumps(data,indent=1)
    new_file_path = file_path[:-4]
    json_file = new_file_path + ".json"
    file = open(json_file, 'w+')
    if json_data != None:
        file.write(json_data)
    return json_file





