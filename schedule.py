#!/usr/bin/python3
#author: YU Fan
#school: HKUST
#The script is used to schedule the benchmark process so that it can run in certain time and certain date.
import sys
import datetime
import subprocess
from settings import current_dir
from apscheduler.schedulers.blocking import BlockingScheduler
def runBenchmark():
    runall = "python3 "+current_dir+"/runall.py"
    #runall = "python3 ../../test/testApp.py"
    proc = subprocess.Popen(runall.split())
    with open(current_dir+'/chart/pid.txt','w') as f:
        f.write(str(proc.pid))

sched = BlockingScheduler()

#This is only for testing
@sched.scheduled_job('interval', seconds=20)
def timed_job():
    with open(current_dir + '/chart/schedule.txt','w') as f:
        f.write('This job is run at '+str(datetime.datetime.now()))

#This is used to schedule benchmark process.
#day_of_week='mon-sun', hour=22,minute=0 means run it at 22：00 from monday to sunday
@sched.scheduled_job('cron', day_of_week='mon-sun', hour=22,minute=0)
def scheduled_job():
    with open(current_dir + '/chart/schedule2.txt', 'w') as f:
        f.write('Benchexec starts running at ' + str(datetime.datetime.now()))
    runBenchmark()

#sched.configure(options_from_ini_file)
sched.start()




